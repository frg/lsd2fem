#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <map>
#include <set>
#include <stdexcept>
#include <vector>

#include <Eigen/Core>
#include <Eigen/Geometry>

struct Node
{
  int nid;      // node id
  double x,y,z; // coordinates
  int tc;       // translation constraint
  int rc;       // rotation constraint
};

struct Element
{
  enum Type : int { SHELL=0, BEAM, SOLID, DISCRETE, SEATBELT } type;
  int eid;            // element id
  int pid;            // part id
  std::vector<int> n; // nodal points
  // card 1 for discrete element
  int vid;            // orientation option
  double s;           // scale factor on forces
  int pf;             // print flag
  double offset;      // initial offset
  // card 2 for shell element
  double beta;        // material frame orientation
};

struct DiscreteMass
{
  enum Type : int { NODE=0, NODE_SET, PART, PART_SET } type;
  int eid;
  int id;
  double mass;
  int pid;
};

struct RigidElement
{
  enum Type : int { NODAL_RIGID_BODY=0 } type;
  std::string title;
  int pid;    // part id
  int cid;    // optional coordinate system id for the rigid body local system
  int nsid;   // nodal set id
  int pnode;  // optional node used for post processing rigid body data
  int iprt;   // print flag
  int drflag; // displacement release flag
  int rrflag; // rotation release flag
};

struct RigidBody
{
  int pidm;  // master rigid body part id
  int pids;  // slave rigid body part id
  int iflag; // inertial property flag
};

struct ConstrainedExtraNodes
{
  int pid;   // part id of rigid body
  int nid;   // node id
  int nsid;  // nodeset id
  int iflag; // inertial property flag
};

struct Set
{
  enum Type : int { NODE=0, BEAM=1, PART=2 } type;
  std::string title;
  int sid;               // set id
  double da1;            // first nodal attribute default value
  double da2;            // second nodal attribute default value
  double da3;            // third nodal attribute default value
  double da4;            // fourth nodal attribute default value
  std::string solver;    // name of solver using this set
  // list option (card 2a)
  std::vector<int> list; // list of node in the set
  // general option (card 2e)
  std::vector<int> parts;
};

struct Joint
{
  enum Type : int { SPHERICAL=0, REVOLUTE, CYLINDRICAL, UNIVERSAL, SPOTWELD } type;
  int jid;             // joint or weld id
  std::string heading; // joint descriptor
  int n1;              // node 1, in rigid body A
  int n2;              // node 2, in rigid body B
  int n3;              // node 3, in rigid body A
  int n4;              // node 4, in rigid body B
  int n5;              // node 5, in rigid body A
  int n6;              // node 6, in rigid body B
  double rps;          // relative penalty stiffness
  double damp;         // damping scale factor
  // card 1 for spotweld
  double sn;           // node 3, in rigid body A
  double ss;           // node 4, in rigid body B
  double n;            // node 5, in rigid body A
  double m;            // node 6, in rigid body B
  double tf;           // relative penalty stiffness
  double ep;           // damping scale factor
};

struct Part
{
  std::string heading;
  int pid;    // part id
  int secid;  // section id
  int mid;    // material id
  int eosid;  // equation of state id
  int hgid;   // hourglass/bulk viscosity id
  int grav;   // flag to turn on gravity initialization
  int adpopt; // indicate if this part is adapted or not
  int tmid;   // thermal material property id
  // inertia card 1
  double xc;  // global x-coordinate of center of mass
  double yc;  // global y-coordinate of center of mass
  double zc;  // global z-coordinate of center of mass
  double tm;  // translational mass
  int ircs;   // flag for inertia tensor reference coordinate system
  int nodeid; // nodal point defining the CG of the rigid body
  // inertia card 2
  double ixx; // xx component of inertia tensor
  double ixy; // xy component of inertia tensor
  double ixz; // xz component of inertia tensor
  double iyy; // yy component of inertia tensor
  double iyz; // yz component of inertia tensor
  double izz; // zz component of inertia tensor
  // inertia card 4
  int cid;    // local coordinate system id
};

struct Section
{
  enum Type : int { SHELL=0, BEAM=1, SOLID=2, DISCRETE=3, SEATBELT=4 } type;
  std::string title;
  // card 1 for shell
  int secid;     // section id
  int elform;    // element formulation options
  double shrf;   // shear correction factor
  double nip;    // number of through thickness integration points
  double propt;  // printout option
  double qr;     // quadrature rule
  int icomp;     // flag for orthotropic/anisotropic layered composite material model
  int setyp;     // not used
  // card 2 for shell
  double t1;     // shell thickness at node 1
  double t2;     // shell thickness at node 2
  double t3;     // shell thickness at node 3
  double t4;     // shell thickness at node 4
  double nloc;   // location of reference surface (shell mid-thickness) for three dimensional shell elements
  double marea;  // non-structural mass per unit area
  double idof;   // treatment of through thickness strain
  int edgset;    // edge node set required for shell type seatbelts
  // card 1 for beam
  double cst;    // cross section type
  double scoor;  // local coordinate system update
  double nsm;    // non-structural mass per unit length
  // card 2 for beam type 1 or 11
  double ts1;    // beam thickness (CST = 0.0, 2.0) or outer diameter (CST = 1.0) in s direction at node 1
  double ts2;    // beam thickness (CST = 0.0, 2.0) or outer diameter (CST = 1.0) in s direction at node 2
  double tt1;    // beam thickness (CST = 0.0, 2.0) or inner diameter (CST = 1.0) in t direction at node 1
  double tt2;    // beam thickness (CST = 0.0, 2.0) or inner diameter (CST = 1.0) in t direction at node 2
  double nsloc;  // location of reference surface normal to s axis for Hughes-Liu beam elements only
  double ntloc;  // location of reference surface normal to t axis for Hughes-Liu beam elements only
  // card 2 for beam type 3
  double a;      // cross-sectional area
  double rampt;  // optional ramp-up time for dynamic relaxation
  double stress; // optional initial stress for dynamic relaxation
  // card 2 for beam type 6
  double vol;    // volume of discrete beam and scalar beam
  double iner;   // mass moment of inertia for the six degree of freedom discrete beam and scalar beam
  double cid;    // coordinate system ID for orientation
  double ca;     // cable area
  double offset; // optional offset for cable
  double rrcon;  // r-rotational constraint for local coordinate system
  double srcon;  // s-rotational constraint for local coordinate system
  double trcon;  // t-rotational constraint for local coordinate system
  // card 2 for beam type 9
  double print;  // output spot force resultant from spotwelds
  double itoff;  // option to specify torsional behavior for spotweld beams
  // card 1 for solid
  int aet;       // ambient element type
  // card 1 for discrete
  double dro;    // displacement/rotation option
  int kd;        // dynamic magnification factor
  int v0;        // test velocity
  int cl;        // clearance
  int fd;        // failure deflection/twist
  // card 2 for discrete
  double cdl;    // deflection/twist limit in compression
  double tdl;    // deflection/twist limit in tension
};

struct Material
{
  enum Type : int { ELASTIC=0, PLASTIC_KINEMATIC, RIGID, PIECEWISE_LINEAR_PLASTICITY, NONLINEAR_ELASTIC_DISCRETE_BEAM, NONLINEAR_PLASTIC_DISCRETE_BEAM,
                    SPOTWELD, MODIFIED_PIECEWISE_LINEAR_PLASTICITY, MODIFIED_HONEYCOMB, DAMPER_VISCOUS, SPRING_NONLINEAR_ELASTIC, SEATBELT, FABRIC } type;
  std::string title;
  // card 1 for elastic material
  int mid;   // material identification
  double ro; // mass density
  double e;  // Young's modulus
  double pr; // Poisson's ratio
  double da; // axial damping factor
  double db; // bending damping factor
  double k;  // bulk modulus
  // card 1 for kinematic plastic material
  double sigy; // yield stress
  double etan; // tangent modulus
  double beta; // hardening parameter
  // card 2 for kinematic plastic material
  double src; // strain rate parameter C
  double srp; // strain rate parameter P
  double fs;  // effective plastic strain for eroding elements
  double vp;  // formulation for rate effects
  // card 1 for rigid material
  double n;          // coupling flag
  double couple;     // coupling option
  double m;          // another coupling flag
  std::string alias; // surface alias name
  // card 2 for rigid material
  double cmo;  // center of mass constraint option
  double con1; // first constraint parameter
  double con2; // second constraint parameter
  // card 3 for rigid material
  double lco;  // local coordinate system for output or 1st component of vector "a" used for specifying local coordinate system
  double a2;   // 2nd component of vector "a" used for specifying local coordinate system
  double a3;   // 3rd component of vector "a" used for specifying local coordinate system
  double v1;   // 1st component of vector "v" used for specifying local coordinate system
  double v2;   // 2nd component of vector "v" used for specifying local coordinate system
  double v3;   // 3rd component of vector "v" used for specifying local coordinate system
  // card 1 for piecewise linear plasticity
  double fail; // failure flag
  double tdel; // minimum time-step
  // card 2 for piecewise linear plasticity
  double c;    // strain rate parameter c
  double p;    // strain rate parameter p
  double lcss; // load curve id defining effective stress versus effective plastic strain, or table id (see figure M24-1)
  double lcsr; // load curve id defining strain rate scaling effect on yield stress
  // card 3 for piecewise linear plasticity
  std::array<double, 8> eps; // effective plastic strain values (optional)
  // card 4 for piecewise linear plasticity
  std::array<double, 8> es; // yield stress values corresponding to eps
  // card 1 for nonlinear elastic discrete beam
  double lcidtr; // load curve id defining translational force resultant along local r-axis versus relative translational displacement
  double lcidts; // load curve id defining translational force resultant along local s-axis versus relative translational displacement
  double lcidtt; // load curve id defining translational force resultant along local t-axis versus relative translational displacement
  double lcidrr; // load curve id defining rotational moment resultant about local r-axis versus relative rotational displacement
  double lcidrs; // load curve id defining rotational moment resultant about local s-axis versus relative rotational displacement
  double lcidrt; // load curve id defining rotational moment resultant about local t-axis versus relative rotational displacement
  // card 2 for nonlinear elastic discrete beam
  double lcidtdr; // load curve id defining translational damping force resultant along local r-axis versus relative translational velocity
  double lcidtds; // load curve id defining translational damping force resultant along local s-axis versus relative translational velocity
  double lcidtdt; // load curve id defining translational damping force resultant along local t-axis versus relative translational velocity
  double lcidrdr; // load curve id defining rotational damping moment resultant about local r-axis versus relative rotational velocity
  double lcidrds; // load curve id defining rotational damping moment resultant about local s-axis versus relative rotational velocity
  double lcidrdt; // load curve id defining rotational damping moment resultant about local t-axis versus relative rotational velocity
  // card 1 for nonlinear plastic discrete beam
  double tkr; // translational stiffness along local r-axis
  double tks; // translational stiffness along local s-axis
  double tkt; // translational stiffness along local t-axis
  double rkr; // rotational stiffness along local r-axis
  double rks; // rotational stiffness along local s-axis
  double rkt; // rotational stiffness along local t-axis
  // card 2 for nonlinear plastic discrete beam
  double tdr; // translational viscous damper along local r-axis
  double tds; // translational viscous damper along local s-axis
  double tdt; // translational viscous damper along local t-axis
  double rdr; // rotational viscous damper along local r-axis
  double rds; // rotational viscous damper along local s-axis
  double rdt; // rotational viscous damper along local t-axis
  // card 2 for modified piecewise linear plasticity
  double epsthin; // thinning strain at failure
  double epsmaj;  // major in-plane strain at failure for shells, or major principal strain at failure for solids
  double numint;  // number of integration points which must fail before the element is deleted
  // card 5 for modified piecewise linear plasticity
  int lctsrf;   // curve that defines the thinning strain at failure as a function of the plastic strain rate
  double eps0;  // parameter for RTCL damage
  double triax; // RTCL damage triaxiality limit
  // card 1 for modified honeycomb
  double vf;   // relative volume at which the honeycomb is fully compacted
  double mu;   // material viscosity coefficient
  double bulk; // bulk viscosity flag
  // card 2 for modified honeycomb
  double lca, lcb, lcc, lcs, lcab, lcbc, lcca; // various load curve ids
  // card 3 for modified honeycomb
  double eaau, ebbu, eccu; // elastic modulii
  double gabu, gbcu, gcau; // shear modulii
  double aopt;             // material axes option
  int macf;                // material axes change flag
  // card 4 for modified honeycomb
  double xp, yp, zp; // coordinates of point p
  double a1;         // first component of vector a
  // card 5 for modified honeycomb
  double d1, d2, d3; // components of vector d
  double tsef;       // tensile strain at element failure
  double ssef;       // shear strain at element failure
  double vref;       // relative volume at which the reference geometry is stored
  double tref;       // element time step size at which the reference geometry is stored
  double shdflg;     // flag defining treatment of damage
  // card 7 for modified honeycomb
  double lcsra, lcsrb, lcsrc, lcsrab, lcsrbc, lcsca; // more load curve ids
  // card 1 for spotweld
  double eh;    // plastic hardening modulus
  double dt;    // time-step size for mass scaling
  double tfail; // failure time if non-zero
  // card 2 for spotweld
  double efail; // effective plastic strain in weld material at failure
  double nrr;   // axial force resultant at failure
  double nrs;   // force resultant at failure
  double nrt;   // force resultant at failure
  double mrr;   // torsional moment resultant at failure
  double mss;   // moment resultant at failure
  double mtt;   // moment resultant at failure
  double nf;    // number of force vectors stored for filtering
  // card 1 for viscous damper
  double dc; // damping constant
  // card 1 for nonlinear elastic spring
  int lcd; // force vs displacement or moment vs rotation load curve id
  int lcr; // optional load curve id for force/moment scale factor vs velocity
  // card 1 for seatbelt
  double mpul; // mass per unit length
  int llcid;   // curve or table id for loading
  int ulcid;   // curve or table id for unloading
  // card 1 for fabric
  double ea, eb;     // Young's modulii
  double prba, prab; // Poisson's ratios
  // card 2 for fabric
  double gab; // shear modulus
};

struct Curve {
  enum Usage : int { MFTT=0, YSST, YSSRT };
  std::string title;
  int lcid;    // load curve id
  int sidr;    // flag controlling use of curve during dynamic relaxation
  double sfa;  // scale factor for abscissa value
  double sfo;  // scale factor for ordinate value
  double offa; // offset for abscissa values
  double offo; // offset for ordinate values
  int dattyp;  // data type
  int lcint;   // number of discretization points
  std::vector<double> a; // abscissa values
  std::vector<double> o; // ordinate values
};

struct Table {
  std::string title;
  int tbid;    // table id
  double sfa;  // scale factor for abscissa value
  double offa; // offset for abscissa values
  std::vector<double> values; // load curve will be defined corresponding to this value
  std::vector<int> curves;
};

struct Boundary {
  int nid;   // node id
  int cid;   // co-ordinate system id
  int dofx;  // insert 1 for translational constraint in local x-direction
  int dofy;  // insert 1 for translational constraint in local y-direction
  int dofz;  // insert 1 for translational constraint in local z-direction
  int dofrx; // insert 1 for rotational constraint about local x-axis
  int dofry; // insert 1 for rotational constraint about local y-axis
  int dofrz; // insert 1 for rotational constraint about local z-axis
};

int main(int argc, char **argv)
{
  if(argc != 3) { std::cerr << "usage: ./a.out <input_file> <output_file>\n"; exit(-1); }

  // local variables
  std::string line;
  std::vector<Node> nodes;
  std::map<int, int> node_renum;
  std::vector<Element> elements;
  std::map<int, int> element_renum;
  std::vector<DiscreteMass> discrete_masses;
  std::vector<RigidElement> rigid_elements;
  std::vector<RigidBody> rigid_bodies;
  std::vector<ConstrainedExtraNodes> constrained_extra_nodes;
  std::vector<Set> sets;
  std::map<int, int> set_renum;
  std::vector<Joint> joints;
  std::map<int, int> joint_renum;
  std::vector<Part> parts;
  std::map<int, int> part_renum;
  std::map<int, Section> sections;
  std::map<int, Material> materials;
  std::map<int, Curve> curves;
  std::map<int, Curve::Usage> curves_usage;
  std::map<int, Table> tables;
  std::vector<Boundary> boundaries;
  int table_id = -1;
  enum Keyword : int { NODE=0, ELEMENT_SHELL, ELEMENT_BEAM, ELEMENT_SOLID,
                       ELEMENT_DISCRETE, ELEMENT_MASS, ELEMENT_MASS_PART, ELEMENT_SEATBELT,
                       SET_NODE_LIST, SET_NODE_GENERAL, SET_BEAM, SET_PART_LIST,
                       CONSTRAINED_NODAL_RIGID_BODY, CONSTRAINED_SPOTWELD,
                       CONSTRAINED_JOINT_SPHERICAL, CONSTRAINED_JOINT_REVOLUTE,
                       CONSTRAINED_JOINT_CYLINDRICAL, CONSTRAINED_JOINT_UNIVERSAL,
                       CONSTRAINED_RIGID_BODIES, CONSTRAINED_EXTRA_NODES,
                       PART, SECTION_SHELL, SECTION_BEAM, SECTION_SOLID, SECTION_DISCRETE, SECTION_SEATBELT,
                       MAT_ELASTIC, MAT_PLASTIC_KINEMATIC, MAT_RIGID,
                       MAT_PIECEWISE_LINEAR_PLASTICITY, MAT_NONLINEAR_ELASTIC_DISCRETE_BEAM,
                       MAT_NONLINEAR_PLASTIC_DISCRETE_BEAM, MAT_SPOTWELD,
                       MAT_MODIFIED_PIECEWISE_LINEAR_PLASTICITY, MAT_MODIFIED_HONEYCOMB,
                       MAT_DAMPER_VISCOUS, MAT_SPRING_NONLINEAR_ELASTIC, MAT_SEATBELT, MAT_FABRIC,
                       DEFINE_CURVE, DEFINE_TABLE, DEFINE_HEX_SPOTWELD_ASSEMBLY_8,
                       BOUNDARY_SPC, KEYWORD, END, NOT_TRANSLATED } keyword;
  int count[NOT_TRANSLATED+1] = {0};
  int wrn = 0;
  std::set<std::string> options;
  bool title;

  // lambda expression for extracting a substring from the current line and converting it to a floating point number, if possible
  // if extraction and/or conversion is not possible, a default value is returned
  auto getdbl = [&](std::string::size_type pos, std::string::size_type count, double _d = 0) -> double {
    try { return std::stod(line.substr(pos, count)); }
    catch(std::invalid_argument) { return _d; }
    catch(std::out_of_range) { return _d; }
  };
  // lambda expression for extracting a substring from the current line and converting it to an integer, if possible
  // if extraction and/or conversion is not possible, a default value is returned
  auto getint = [&](std::string::size_type pos, std::string::size_type count, int _i = 0) -> int {
    try { return std::stoi(line.substr(pos, count)); }
    catch(std::invalid_argument) { return _i; }
    catch(std::out_of_range) { return _i; }
  };
  // lambda expression for extracting a substring from the current line, if possible
  // if extraction is not possible, a default value is returned
  auto getstr = [&](std::string::size_type pos, std::string::size_type count, std::string _s = "") -> std::string {
    try { return line.substr(pos, count); }
    catch(std::out_of_range) { return _s; }
  };

  // open input file
  std::ifstream in(argv[1]);

  // read first line
  int line_number = 0;
  std::getline(in, line);

  // lambda expression for reading a new non-comment line and incrementing the line_number
  auto getlin = [&]() -> void {
    do {
      line_number++;
      std::getline(in, line);
    } while(line[0] == '$');
  };

  // main loop
  while(!in.eof()) {
    bool read_next = true;
    if(line.empty() || line[0] == '$') {}
    else if(line.starts_with("*NODE") && !line.starts_with("*NODE_MERGE_SET") && !line.starts_with("*NODE_MERGE_TOLERANCE")
                                      && !line.starts_with("*NODE_RIGID_SURFACE") && !line.starts_with("*NODE_SCALAR")
                                      && !line.starts_with("*NODE_THICKNESS") && !line.starts_with("*NODE_TO_TARGET_VECTOR")
                                      && !line.starts_with("*NODE_TRANSFORM")) {
      keyword = NODE;
      options.clear();
    }
    else if(line.starts_with("*ELEMENT_SHELL") && !line.starts_with("*ELEMENT_SHELL_NURBS_PATCH") && !line.starts_with("*ELEMENT_SHELL_SOURCE_SINK")) {
      keyword = ELEMENT_SHELL;
      options.clear();
      if(line.find("_BETA") != std::string::npos) options.insert("BETA");
    }
    else if(line.starts_with("*ELEMENT_BEAM") && !line.starts_with("*ELEMENT_BEAM_PULLEY") && !line.starts_with("*ELEMENT_BEAM_SOURCE")) {
      keyword = ELEMENT_BEAM;
      options.clear();
    }
    else if(line.starts_with("*ELEMENT_SOLID") && !line.starts_with("*ELEMENT_SOLID_NURBS_PATCH")) {
      keyword = ELEMENT_SOLID;
      options.clear();
    }
    else if(line.starts_with("*ELEMENT_DISCRETE") && !line.starts_with("*ELEMENT_DISCRETE_SPHERE")) {
      keyword = ELEMENT_DISCRETE;
      options.clear();
    }
    else if(line.starts_with("*ELEMENT_SEATBELT") && !line.starts_with("*ELEMENT_SEATBELT_ACCELEROMETER") && !line.starts_with("*ELEMENT_SEATBELT_PRETENSIONER")) {
      keyword = ELEMENT_SEATBELT;
      options.clear();
    }
    else if(line.starts_with("*ELEMENT_MASS") && !line.starts_with("*ELEMENT_MASS_MATRIX") && !line.starts_with("*ELEMENT_MASS_PART")) {
      keyword = ELEMENT_MASS;
      options.clear();
      if(line.find("_NODE_SET") != std::string::npos) options.insert("NODE_SET");
    }
    else if(line.starts_with("*ELEMENT_MASS_PART")) {
      keyword = ELEMENT_MASS_PART;
      options.clear();
      if(line.find("_PART_SET") != std::string::npos) options.insert("PART_SET");
    }
    else if(line.starts_with("*SET_NODE_LIST") && !line.starts_with("*SET_NODE_LIST_GENERATE")) {
      keyword = SET_NODE_LIST;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*SET_NODE_GENERAL")) {
      keyword = SET_NODE_GENERAL;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*SET_PART_LIST") && !line.starts_with("*SET_PART_LIST_GENERATE")) {
      keyword = SET_PART_LIST;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*SET_BEAM") && !line.starts_with("*SET_BEAM_GENERATE") && !line.starts_with("*SET_BEAM_GENERAL")) {
      keyword = SET_BEAM;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*CONSTRAINED_NODAL_RIGID_BODY")) {
      keyword = CONSTRAINED_NODAL_RIGID_BODY;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*CONSTRAINED_RIGID_BODIES")) {
      keyword = CONSTRAINED_RIGID_BODIES;
      options.clear();
    }
     else if(line.starts_with("*CONSTRAINED_EXTRA_NODES")) {
      keyword = CONSTRAINED_EXTRA_NODES;
      options.clear();
      if(line.find("NODES_NODE") != std::string::npos) options.insert("NODE");
      else if(line.find("NODES_SET") != std::string::npos) options.insert("SET");
    }
    else if(line.starts_with("*CONSTRAINED_SPOTWELD")) {
      keyword = CONSTRAINED_SPOTWELD;
      options.clear();
      if(line.find("_ID") != std::string::npos) options.insert("ID");
    }
    else if(line.starts_with("*CONSTRAINED_JOINT_SPHERICAL")) {
      keyword = CONSTRAINED_JOINT_SPHERICAL;
      options.clear();
      if(line.find("_ID") != std::string::npos) options.insert("ID");
      if(line.find("_FAILURE") != std::string::npos) options.insert("FAILURE");
    }
    else if(line.starts_with("*CONSTRAINED_JOINT_REVOLUTE")) {
      keyword = CONSTRAINED_JOINT_REVOLUTE;
      options.clear();
      if(line.find("_ID") != std::string::npos) options.insert("ID");
      if(line.find("_FAILURE") != std::string::npos) options.insert("FAILURE");
    }
    else if(line.starts_with("*CONSTRAINED_JOINT_CYLINDRICAL")) {
      keyword = CONSTRAINED_JOINT_CYLINDRICAL;
      options.clear();
      if(line.find("_ID") != std::string::npos) options.insert("ID");
      if(line.find("_FAILURE") != std::string::npos) options.insert("FAILURE");
    }
    else if(line.starts_with("*CONSTRAINED_JOINT_UNIVERSAL")) {
      keyword = CONSTRAINED_JOINT_UNIVERSAL;
      options.clear();
      if(line.find("_ID") != std::string::npos) options.insert("ID");
      if(line.find("_FAILURE") != std::string::npos) options.insert("FAILURE");
    }
    else if(line.starts_with("*PART") && !line.starts_with("*PART_ADAPTIVE_FAILURE") && !line.starts_with("*PART_ANNEAL")
                                      && !line.starts_with("*PART_COMPOSITE") && !line.starts_with("*PART_DUPLICATE")
                                      && !line.starts_with("*PART_MODES") && !line.starts_with("*PART_MOVE")
                                      && !line.starts_with("*PART_SENSOR") && !line.starts_with("*PART_STACKED_ELEMENTS")) {
      keyword = PART;
      options.clear();
      if(line.find("_INERTIA") != std::string::npos) options.insert("INERTIA");
    }
    else if(line.starts_with("*SECTION_SHELL")) {
      keyword = SECTION_SHELL;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*SECTION_BEAM") && !line.starts_with("*SECTION_BEAM_AISC")) {
      keyword = SECTION_BEAM;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*SECTION_SOLID")) {
      keyword = SECTION_SOLID;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*SECTION_DISCRETE")) {
      keyword = SECTION_DISCRETE;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*SECTION_SEATBELT")) {
      keyword = SECTION_SEATBELT;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_ELASTIC") || line.starts_with("*MAT_001")) {
      keyword = MAT_ELASTIC;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_PLASTIC_KINEMATIC") || line.starts_with("*MAT_003")) {
      keyword = MAT_PLASTIC_KINEMATIC;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_RIGID") || line.starts_with("*MAT_020")) {
      keyword = MAT_RIGID;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_PIECEWISE_LINEAR_PLASTICITY") || line.starts_with("*MAT_024")) {
      keyword = MAT_PIECEWISE_LINEAR_PLASTICITY;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_NONLINEAR_ELASTIC_DISCRETE_BEAM") || line.starts_with("*MAT_067")) {
      keyword = MAT_NONLINEAR_ELASTIC_DISCRETE_BEAM;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_NONLINEAR_PLASTIC_DISCRETE_BEAM") || line.starts_with("*MAT_068")) {
      keyword = MAT_NONLINEAR_PLASTIC_DISCRETE_BEAM;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_SPOTWELD") || line.starts_with("*MAT_100")) {
      keyword = MAT_SPOTWELD;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_MODIFIED_PIECEWISE_LINEAR_PLASTICITY") || line.starts_with("*MAT_123")) {
      keyword = MAT_MODIFIED_PIECEWISE_LINEAR_PLASTICITY;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_MODIFIED_HONEYCOMB") || line.starts_with("*MAT_126")) {
      keyword = MAT_MODIFIED_HONEYCOMB;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_DAMPER_VISCOUS") || line.starts_with("*MAT_S02")) {
      keyword = MAT_DAMPER_VISCOUS;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_SPRING_NONLINEAR_ELASTIC") || line.starts_with("*MAT_S04")) {
      keyword = MAT_SPRING_NONLINEAR_ELASTIC;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_SEATBELT") || line.starts_with("*MAT_B01")) {
      keyword = MAT_SEATBELT;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*MAT_FABRIC") || line.starts_with("*MAT_034")) {
      keyword = MAT_FABRIC;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*DEFINE_CURVE") && !line.starts_with("*DEFINE_CURVE_BOX_ADAPTIVITY")
                                              && !line.starts_with("*DEFINE_CURVE_COMPENSATION_CONSTRAINT")
                                              && !line.starts_with("*DEFINE_CURVE_DRAWBEAD")
                                              && !line.starts_with("*DEFINE_CURVE_DUPLICATE") 
                                              && !line.starts_with("*DEFINE_CURVE_ENTITY") 
                                              && !line.starts_with("*DEFINE_CURVE_FEEDBACK") 
                                              && !line.starts_with("*DEFINE_CURVE_FLC") 
                                              && !line.starts_with("*DEFINE_CURVE_FLD_FROM_TRIAXIAL_LIMIT") 
                                              && !line.starts_with("*DEFINE_CURVE_FUNCTION") 
                                              && !line.starts_with("*DEFINE_CURVE_SMOOTH") 
                                              && !line.starts_with("*DEFINE_CURVE_STRESS")
                                              && !line.starts_with("*DEFINE_CURVE_TRIAXIAL_LIMIT_FROM_FLD")
                                              && !line.starts_with("*DEFINE_CURVE_TRIM")) {
      keyword = DEFINE_CURVE;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*DEFINE_TABLE") && !line.starts_with("*DEFINE_TABLE_2D")
                                              && !line.starts_with("*DEFINE_TABLE_3D")
                                              && !line.starts_with("*DEFINE_TABLE_MATRIX")) {
      keyword = DEFINE_TABLE;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*DEFINE_HEX_SPOTWELD_ASSEMBLY_8")) {
      keyword = DEFINE_HEX_SPOTWELD_ASSEMBLY_8;
      options.clear();
      title = (line.find("_TITLE") != std::string::npos);
    }
    else if(line.starts_with("*BOUNDARY_SPC\0")) {
      keyword = BOUNDARY_SPC;
      options.clear();
      if(line.find("_NODE") != std::string::npos) options.insert("NODE");
    }
    else if(line.starts_with("*KEYWORD\0")) {
      keyword = KEYWORD;
      options.clear();
    }
    else if(line.starts_with("*END\0")) {
      keyword = END;
      options.clear();
    }
    else if(line.starts_with("*")) {
      std::cerr << "line #" << line_number << " not translated: " << line << std::endl;
      keyword = NOT_TRANSLATED;
    }
    else {
      switch(keyword) {
        case NODE : {
          Node n;
          n.nid = getint(0, 8);
          n.x   = getdbl(8, 16);
          n.y   = getdbl(24, 16);
          n.z   = getdbl(40, 16);
          n.tc  = getint(56, 8);
          n.rc  = getint(64, 8);
          nodes.push_back(n);
        } break;
        case ELEMENT_SHELL : {
          Element e;
          e.type = Element::SHELL;
          e.eid  = getint(0, 8);
          e.pid  = getint(8, 8);
          e.n.reserve(4);
          e.n.push_back(getint(16, 8));
          e.n.push_back(getint(24, 8));
          e.n.push_back(getint(32, 8));
          int n4 = getint(40, 8); if(n4 != e.n.back()) e.n.push_back(n4);
          if(options.contains("BETA")) {
            getlin();
            e.beta = getdbl(64, 16);
          }
          else e.beta = 0;
          elements.push_back(e);
        } break;
        case ELEMENT_BEAM : {
          Element e;
          e.type = Element::BEAM;
          e.eid  = getint(0, 8);
          e.pid  = getint(8, 8);
          e.n.reserve(2);
          e.n.push_back(getint(16, 8));
          e.n.push_back(getint(24, 8));
          elements.push_back(e);
        } break;
        case ELEMENT_SOLID : {
          Element e;
          e.type = Element::SOLID;
          e.eid  = getint(0, 8);
          e.pid  = getint(8, 8);
          getlin();
          e.n.reserve(8);
          e.n.push_back(getint(0, 8));
          e.n.push_back(getint(8, 8));
          e.n.push_back(getint(16, 8));
          e.n.push_back(getint(24, 8));
          int n5 = getint(32, 8); if(n5 != e.n.back()) e.n.push_back(n5);
          int n6 = getint(40, 8); if(n6 != e.n.back()) e.n.push_back(n6);
          int n7 = getint(48, 8); if(n7 != e.n.back()) e.n.push_back(n7);
          int n8 = getint(56, 8); if(n8 != e.n.back()) e.n.push_back(n8);
          if(e.n.size() == 6) { // reorder 6-node penta
            std::vector<int> nn = {e.n[0], e.n[4], e.n[1], e.n[3], e.n[5], e.n[2]};
            e.n = nn;
          }
          elements.push_back(e);
        } break;
        case ELEMENT_DISCRETE : {
          Element e;
          e.type = Element::DISCRETE;
          e.eid  = getint(0, 8);
          e.pid  = getint(8, 8);
          e.n.reserve(2);
          e.n.push_back(getint(16, 8));
          e.n.push_back(getint(24, 8));
          e.vid  = getint(32, 8);
          e.s    = getdbl(40, 16, 1.0);
          e.pf   = getint(56, 8);
          e.offset = getdbl(64, 16);
          elements.push_back(e);
        } break;
        case ELEMENT_SEATBELT : {
          Element e;
          e.type = Element::SEATBELT;
          e.eid  = getint(0, 8);
          e.pid  = getint(8, 8);
          e.n.reserve(2);
          e.n.push_back(getint(16, 8));
          e.n.push_back(getint(24, 8));
          elements.push_back(e);
        } break;
        case ELEMENT_MASS : {
          DiscreteMass d;
          d.type = options.contains("NODE_SET") ? DiscreteMass::NODE_SET : DiscreteMass::NODE;
          d.eid  = getint(0, 8);
          d.id   = getint(8, 8);
          d.mass = getdbl(16, 16);
          d.pid  = getint(32, 8);
          discrete_masses.push_back(d);
        } break;
        case ELEMENT_MASS_PART : {
          DiscreteMass d;
          d.type = options.contains("PART_SET") ? DiscreteMass::PART_SET : DiscreteMass::PART;
          d.id   = getint(0, 8);
          d.mass = getdbl(16, 16);
          discrete_masses.push_back(d);
        } break;
        case SET_NODE_LIST : {
          Set s;
          s.type = Set::NODE;
          if(title) {
            s.title = getstr(0, 80);
            getlin();
          }
          s.sid  = getint(0, 10);
          s.da1  = getdbl(10, 10);
          s.da2  = getdbl(20, 10);
          s.da3  = getdbl(30, 10);
          s.da4  = getdbl(40, 10);
          s.solver = getstr(50, 10);
          while(true) {
            line_number++;
            std::getline(in, line);
            if(line[0] == '$' || line[0] == '*') {
              read_next = false;
              break;
            }
            for(int i = 0; i < 8; ++i) {
              int n = getint(10*i, 10, -1);
              if(n != -1) s.list.push_back(n);
            }
          }
          sets.push_back(s);
        } break;
        case SET_NODE_GENERAL : {
          Set s;
          s.type = Set::NODE;
          if(title) {
            s.title = getstr(0, 80);
            getlin();
          }
          s.sid = getint(0, 10);
          s.da1 = getdbl(10, 10);
          s.da2 = getdbl(20, 10);
          s.da3 = getdbl(30, 10);
          s.da4 = getdbl(40, 10);
          s.solver = getstr(50, 10);
          while(true) {
            line_number++;
            std::getline(in, line);
            if(line[0] == '$' || line[0] == '*') {
              read_next = false;
              break;
            }
            std::string option = getstr(0, 10);
            if(option.starts_with("PART")) {
              for(int i = 1; i < 8; ++i) {
                int pid = getint(10*i, 10, -1);
                if(pid != -1) s.parts.push_back(pid);
              }
            }
            else {
              std::cerr << "*** Warning: node set #" << s.sid << " not translated due to unsupported option " << option << std::endl;
              wrn++;
            }
          }
          sets.push_back(s);
        } break;
        case SET_PART_LIST : {
          Set s;
          s.type = Set::PART;
          if(title) {
            s.title = getstr(0, 80);
            getlin();
          }
          s.sid  = getint(0, 10);
          s.da1  = getdbl(10, 10);
          s.da2  = getdbl(20, 10);
          s.da3  = getdbl(30, 10);
          s.da4  = getdbl(40, 10);
          s.solver = getstr(50, 10);
          while(true) {
            line_number++;
            std::getline(in, line);
            if(line[0] == '$' || line[0] == '*') {
              read_next = false;
              break;
            }
            for(int i = 0; i < 8; ++i) {
              int p = getint(10*i, 10, -1);
              if(p != -1) s.list.push_back(p);
            }
          }
          sets.push_back(s);
        } break;
        case SET_BEAM : {
          Set s;
          s.type = Set::BEAM;
          if(title) {
            s.title = getstr(0, 80);
            getlin();
          }
          s.sid  = getint(0, 10);
          while(true) {
            line_number++;
            std::getline(in, line);
            if(line[0] == '$' || line[0] == '*') {
              read_next = false;
              break;
            }
            for(int i = 0; i < 8; ++i) {
              int k = getint(10*i, 10, -1);
              if(k != -1) s.list.push_back(k);
            }
          }
          sets.push_back(s);
        } break;
        case CONSTRAINED_NODAL_RIGID_BODY : {
          RigidElement r;
          r.type = RigidElement::NODAL_RIGID_BODY;
          if(title) {
            r.title = getstr(0, 80);
            getlin();
          }
          r.pid    = getint(0, 10);
          r.cid    = getint(10, 10);
          r.nsid   = getint(20, 10);
          r.pnode  = getint(30, 10);
          r.iprt   = getint(40, 10);
          r.drflag = getint(50, 10);
          r.rrflag = getint(60, 10);
          rigid_elements.push_back(r);
        } break;
        case CONSTRAINED_RIGID_BODIES : {
          RigidBody r;
          r.pidm  = getint(0, 10);
          r.pids  = getint(10, 10);
          r.iflag = getint(20, 10);
          rigid_bodies.push_back(r);
        } break;
        case CONSTRAINED_EXTRA_NODES : {
          ConstrainedExtraNodes c;
          c.pid   = getint(0, 10);
          if(options.contains("NODE")) {
            c.nid  = getint(10, 10);
            c.nsid = -1;
          }
          else if(options.contains("SET")) {
            c.nid  = -1;
            c.nsid = getint(10, 10);
          }
          c.iflag = getint(20, 10);
          constrained_extra_nodes.push_back(c);
        } break;
        case CONSTRAINED_SPOTWELD : {
          Joint j;
          j.type = Joint::SPOTWELD;
          if(options.contains("ID")) {
            j.jid = getint(0, 10);
            getlin();
          }
          j.n1 = getint(0, 10);
          j.n2 = getint(10, 10);
          j.sn = getdbl(20, 10);
          j.ss = getdbl(30, 10);
          j.n  = getdbl(40, 10);
          j.m  = getdbl(50, 10);
          j.tf = getdbl(60, 10, 1e20);
          j.ep = getdbl(70, 10, 1e20);
          joints.push_back(j);
        } break;
        case CONSTRAINED_JOINT_SPHERICAL : {
          Joint j;
          j.type = Joint::SPHERICAL;
          if(options.contains("ID")) {
            j.jid     = getint(0, 10);
            j.heading = getstr(10, 70);
            getlin();
          }
          j.n1   = getint(0, 10);
          j.n2   = getint(10, 10);
          j.rps  = getdbl(60, 10, 1.0);
          j.damp = getdbl(70, 10, 1.0);
          if(options.contains("FAILURE")) { // TODO
            getlin();
            getlin();
          }
          joints.push_back(j);
        } break;
        case CONSTRAINED_JOINT_REVOLUTE : {
          Joint j;
          j.type = Joint::REVOLUTE;
          if(options.contains("ID")) {
            j.jid     = getint(0, 10);
            j.heading = getstr(10, 70);
            getlin();
          }
          j.n1   = getint(0, 10);
          j.n2   = getint(10, 10);
          j.n3   = getint(20, 10);
          j.n4   = getint(30, 10);
          j.rps  = getdbl(60, 10, 1.0);
          j.damp = getdbl(70, 10, 1.0);
          if(options.contains("FAILURE")) { // TODO
            getlin();
            getlin();
          }
          joints.push_back(j);
        } break;
        case CONSTRAINED_JOINT_CYLINDRICAL : {
          Joint j;
          j.type = Joint::CYLINDRICAL;
          if(options.contains("ID")) {
            j.jid     = getint(0, 10);
            j.heading = getstr(10, 70);
            getlin();
          }
          j.n1   = getint(0, 10);
          j.n2   = getint(10, 10);
          j.n3   = getint(20, 10);
          j.n4   = getint(30, 10);
          j.rps  = getdbl(60, 10, 1.0);
          j.damp = getdbl(70, 10, 1.0);
          if(options.contains("FAILURE")) { // TODO
            getlin();
            getlin();
          }
          joints.push_back(j);
        } break;
        case CONSTRAINED_JOINT_UNIVERSAL : {
          Joint j;
          j.type = Joint::CYLINDRICAL;
          if(options.contains("ID")) {
            j.jid     = getint(0, 10);
            j.heading = getstr(10, 70);
            getlin();
          }
          j.n1   = getint(0, 10);
          j.n2   = getint(10, 10);
          j.n3   = getint(20, 10);
          j.n4   = getint(30, 10);
          j.rps  = getdbl(60, 10, 1.0);
          j.damp = getdbl(70, 10, 1.0);
          if(options.contains("FAILURE")) { // TODO
            getlin();
            getlin();
          }
          joints.push_back(j);
        } break;
        case PART : {
          Part p; 
          p.heading = getstr(0, 70);
          getlin();
          p.pid    = getint(0, 10);
          p.secid  = getint(10, 10);
          p.mid    = getint(20, 10);
          p.eosid  = getint(30, 10);
          p.hgid   = getint(40, 10);
          p.grav   = getint(50, 10);
          p.adpopt = getint(60, 10);
          p.tmid   = getint(70, 10);
          if(options.contains("INERTIA")) {
            getlin(); // card 3
            p.xc     = getdbl(0, 10);
            p.yc     = getdbl(10, 10);
            p.zc     = getdbl(20, 10);
            p.tm     = getdbl(30, 10); 
            p.ircs   = getint(40, 10);
            p.nodeid = getint(50, 10);
            getlin(); // card 4
            p.ixx    = getdbl(0, 10);
            p.ixy    = getdbl(10, 10);
            p.ixz    = getdbl(20, 10);
            p.iyy    = getdbl(30, 10);
            p.iyz    = getdbl(40, 10);
            p.izz    = getdbl(50, 10);
            getlin(); // card 5
            getlin(); // card 6
            p.cid    = getint(60, 10);
          }
          else {
            p.tm = 0;
          }
          parts.push_back(p);
        } break;
        case SECTION_SHELL : {
          Section s;
          s.type = Section::SHELL;
          if(title) {
            s.title = getstr(0, 80);
            getlin();
          }
          s.secid  = getint(0, 10);
          s.elform = getint(10, 10);
          s.shrf   = getdbl(20, 10, 1.0);
          s.nip    = getdbl(30, 10, 2.0);
          s.propt  = getdbl(40, 10, 0.0);
          s.qr     = getdbl(50, 10, 0.0);
          s.icomp  = getint(60, 10, 0);
          s.setyp  = getint(70, 10, 1);
          getlin();
          s.t1     = getdbl(0, 10, 0.0);
          s.t2     = getdbl(10, 10, s.t1);
          s.t3     = getdbl(20, 10, s.t1);
          s.t4     = getdbl(30, 10, s.t1);
          s.nloc   = getdbl(40, 10, 0.0);
          s.marea  = getdbl(50, 10, 0.0);
          s.idof   = getdbl(60, 10, 0.0);
          s.edgset = getint(70, 10, 0);
          sections[s.secid] = s;
        } break;
        case SECTION_BEAM : {
          Section s;
          s.type = Section::BEAM;
          if(title) {
            s.title = getstr(0, 80);
            getlin();
          }
          s.secid  = getint(0, 10);
          s.elform = getint(10, 10, 1);
          s.shrf   = getdbl(20, 10, 1.0);
          s.qr     = getdbl(30, 10, 2.0);
          s.cst    = getdbl(40, 10, 0.0);
          s.scoor  = getdbl(50, 10, 0.0);
          s.nsm    = getdbl(60, 10, 0.0);
          if(s.elform == 1 || s.elform == 11) {
            getlin();
            s.ts1    = getdbl(0, 10);
            s.ts2    = getdbl(10, 10);
            s.tt1    = getdbl(20, 10);
            s.tt2    = getdbl(30, 10);
            s.nsloc  = getdbl(40, 10);
            s.ntloc  = getdbl(50, 10);
          }
          else if(s.elform == 3) {
            getlin();
            s.a      = getdbl(0, 10);
            s.rampt  = getdbl(10, 10);
            s.stress = getdbl(20, 10);
          }
          else if(s.elform == 6) {
            getlin();
            s.vol    = getdbl(0, 10);
            s.iner   = getdbl(10, 10);
            s.cid    = getdbl(20, 10);
            s.ca     = getdbl(30, 10);
            s.offset = getdbl(40, 10);
            s.rrcon  = getdbl(50, 10);
            s.srcon  = getdbl(60, 10);
            s.trcon  = getdbl(70, 10);
          }
          else if(s.elform == 9) {
            getlin();
            s.ts1   = getdbl(0, 10);
            s.ts2   = getdbl(10, 10);
            s.tt1   = getdbl(20, 10);
            s.tt2   = getdbl(30, 10);
            s.print = getdbl(40, 10);
            s.itoff = getdbl(60, 10);
          }
          sections[s.secid] = s;
        } break;
        case SECTION_SOLID : {
          Section s;
          s.type = Section::SOLID;
          if(title) {
            s.title = getstr(0, 80);
            getlin();
          }
          s.secid = getint(0, 10);
          s.aet   = getint(10, 10);
          sections[s.secid] = s;
        } break;
        case SECTION_DISCRETE : {
          Section s;
          s.type = Section::DISCRETE;
          if(title) {
            s.title = getstr(0, 80);
            getlin();
          }
          s.secid = getint(0, 10);
          s.dro   = getdbl(10, 10);
          s.kd    = getint(20, 10);
          s.v0    = getint(30, 10);
          s.cl    = getint(40, 10);
          s.fd    = getint(50, 10);
          getlin();
          s.cdl   = getdbl(0, 10);
          s.tdl   = getdbl(10, 10);
          sections[s.secid] = s;
        } break;
        case SECTION_SEATBELT : {
          Section s;
          s.type = Section::SEATBELT;
          if(title) {
            s.title = getstr(0, 80);
            getlin();
          }
          s.secid = getint(0, 10);
          sections[s.secid] = s;
        } break;
        case MAT_ELASTIC : {
          Material m;
          m.type = Material::ELASTIC;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid = getint(0, 10);
          m.ro  = getdbl(10, 10);
          m.e   = getdbl(20, 10);
          m.pr  = getdbl(30, 10);
          m.da  = getdbl(40, 10);
          m.db  = getdbl(50, 10);
          m.k   = getdbl(60, 10);
          materials[m.mid] = m;
        } break;
        case MAT_PLASTIC_KINEMATIC : {
          Material m;
          m.type = Material::PLASTIC_KINEMATIC;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid  = getint(0, 10);
          m.ro   = getdbl(10, 10);
          m.e    = getdbl(20, 10);
          m.pr   = getdbl(30, 10);
          m.sigy = getdbl(40, 10);
          m.etan = getdbl(50, 10, 0.0);
          m.beta = getdbl(60, 10, 0.0);
          getlin();
          m.src = getdbl(0, 10, 0.0);
          m.srp = getdbl(10, 10, 0.0);
          m.fs  = getdbl(20, 10, 1.e20);
          m.vp  = getdbl(30, 10, 0.0);
          materials[m.mid] = m;
        } break;
        case MAT_RIGID : {
          Material m;
          m.type = Material::RIGID;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid    = getint(0, 10);
          m.ro     = getdbl(10, 10);
          m.e      = getdbl(20, 10);
          m.pr     = getdbl(30, 10);
          m.n      = getdbl(40, 10);
          m.couple = getdbl(50, 10);
          m.m      = getdbl(60, 10);
          m.alias  = getstr(70, 10);
          getlin();
          m.cmo    = getdbl(0, 10);
          m.con1   = getdbl(10, 10);
          m.con2   = getdbl(20, 10);
          getlin();
          m.lco    = getdbl(0, 10);
          m.a2     = getdbl(10, 10);
          m.a3     = getdbl(20, 10);
          m.v1     = getdbl(30, 10);
          m.v2     = getdbl(40, 10);
          m.v3     = getdbl(50, 10);
          materials[m.mid] = m;
        } break;
        case MAT_PIECEWISE_LINEAR_PLASTICITY : {
          Material m;
          m.type = Material::PIECEWISE_LINEAR_PLASTICITY;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid = getint(0, 10);
          m.ro   = getdbl(10, 10);
          m.e    = getdbl(20, 10);
          m.pr   = getdbl(30, 10);
          m.sigy = getdbl(40, 10);
          m.etan = getdbl(50, 10, 0.0);
          m.fail = getdbl(60, 10, 1.e21);
          m.tdel = getdbl(70, 10, 0.0);
          getlin();
          m.c    = getdbl(0, 10, 0.0);
          m.p    = getdbl(10, 10, 0.0);
          m.lcss = getdbl(20, 10, 0.0);
          m.lcsr = getdbl(30, 10, 0.0);
          m.vp   = getdbl(40, 10, 0.0);
          getlin();
          for(int i = 0; i < 8; ++i) {
            m.eps[i] = getdbl(10*i, 10, 0.0);
          }
          getlin();
          for(int i = 0; i < 8; ++i) {
            m.es[i] = getdbl(10*i, 10, 0.0);
          }
          materials[m.mid] = m;
        } break;
        case MAT_NONLINEAR_ELASTIC_DISCRETE_BEAM : {
          Material m;
          m.type = Material::NONLINEAR_ELASTIC_DISCRETE_BEAM;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid    = getint(0, 10);
          m.ro     = getdbl(10, 10);
          m.lcidtr = getdbl(20, 10);
          m.lcidts = getdbl(30, 10);
          m.lcidtt = getdbl(40, 10);
          m.lcidrr = getdbl(50, 10);
          m.lcidrs = getdbl(60, 10);
          m.lcidrt = getdbl(70, 10);
          getlin();
          m.lcidtdr = getdbl(0, 10);
          m.lcidtds = getdbl(10, 10);
          m.lcidtdt = getdbl(20, 10);
          m.lcidrdr = getdbl(30, 10);
          m.lcidrds = getdbl(40, 10);
          m.lcidrdt = getdbl(50, 10);
          getlin(); // TODO: Card 3
          materials[m.mid] = m;
        } break;
        case MAT_NONLINEAR_PLASTIC_DISCRETE_BEAM : {
          Material m;
          m.type = Material::NONLINEAR_PLASTIC_DISCRETE_BEAM;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid = getint(0, 10);
          m.ro  = getdbl(10, 10);
          m.tkr = getdbl(20, 10);
          m.tks = getdbl(30, 10);
          m.tkt = getdbl(40, 10);
          m.rkr = getdbl(50, 10);
          m.rks = getdbl(60, 10);
          m.rkt = getdbl(70, 10);
          getlin();
          m.tdr = getdbl(0, 10);
          m.tds = getdbl(10, 10);
          m.tdt = getdbl(20, 10);
          m.rdr = getdbl(30, 10);
          m.rds = getdbl(40, 10);
          m.rdt = getdbl(50, 10);
          getlin(); // TODO: Card 3
          getlin(); // TODO: Card 4
          getlin(); // TODO: Card 5
          getlin(); // TODO: Card 6
          materials[m.mid] = m;
        } break;
        case MAT_SPOTWELD : {
          Material m;
          m.type = Material::SPOTWELD;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid   = getint(0, 10);
          m.ro    = getdbl(10, 10);
          m.e     = getdbl(20, 10);
          m.pr    = getdbl(30, 10);
          m.sigy  = getdbl(40, 10);
          m.eh    = getdbl(50, 10);
          m.dt    = getdbl(60, 10);
          m.tfail = getdbl(70, 10);
          if(!options.contains("DAMAGE-FAILURE")) {
            getlin();
            m.efail = getdbl(0, 10);
            m.nrr   = getdbl(10, 10);
            m.nrs   = getdbl(20, 10);
            m.nrt   = getdbl(30, 10);
            m.mrr   = getdbl(40, 10);
            m.mss   = getdbl(50, 10);
            m.mtt   = getdbl(60, 10);
            m.nf    = getdbl(70, 10);
          }
          else {
            std::cerr << "*** Warning: MAT_SPOTWELD with DAMAGE-FAILURE option not translated\n";
            wrn++;
            getlin(); // card 2
            getlin(); // card 3
            double opt    = getdbl(10, 10);
            double dmgopt = getdbl(70, 10);
            if(dmgopt == -1) {
              getlin(); // card 3A
            }
            if(opt == 12 || opt == 22) {
              getlin(); // card 4
              getlin(); // card 5
            }
          }
          materials[m.mid] = m;
        } break;
        case MAT_MODIFIED_PIECEWISE_LINEAR_PLASTICITY : {
          Material m;
          m.type = Material::MODIFIED_PIECEWISE_LINEAR_PLASTICITY;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid = getint(0, 10);
          m.ro   = getdbl(10, 10);
          m.e    = getdbl(20, 10);
          m.pr   = getdbl(30, 10);
          m.sigy = getdbl(40, 10);
          m.etan = getdbl(50, 10, 0.0);
          m.fail = getdbl(60, 10, 1.e21);
          m.tdel = getdbl(70, 10, 0.0);
          getlin();
          m.c       = getdbl(0, 10, 0.0);
          m.p       = getdbl(10, 10, 0.0);
          m.lcss    = getdbl(20, 10, 0.0);
          m.lcsr    = getdbl(30, 10, 0.0);
          m.vp      = getdbl(40, 10, 0.0);
          m.epsthin = getdbl(50, 10, 0.0);
          m.epsmaj  = getdbl(60, 10, 0.0);
          m.numint  = getdbl(70, 10, 0.0);
          getlin();
          for(int i = 0; i < 8; ++i) {
            m.eps[i] = getdbl(10*i, 10, 0.0);
          }
          getlin();
          for(int i = 0; i < 8; ++i) {
            m.es[i] = getdbl(10*i, 10, 0.0);
          }
          if(options.contains("RATE") || options.contains("RTCL")) {
            getlin();
            m.lctsrf = getint(0, 10, 0);
            m.eps0   = getdbl(10, 10, 0.0);
            m.triax  = getdbl(20, 10, 0.0);
          }
          materials[m.mid] = m;
        } break;
        case MAT_MODIFIED_HONEYCOMB : {
          Material m;
          m.type = Material::MODIFIED_HONEYCOMB;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid  = getint(0, 10);
          m.ro   = getdbl(10, 10);
          m.e    = getdbl(20, 10);
          m.pr   = getdbl(30, 10);
          m.sigy = getdbl(40, 10);
          m.vf   = getdbl(50, 10);
          m.mu   = getdbl(60, 10, 0.05);
          m.bulk = getdbl(70, 10, 0.0);
          getlin();
          m.lca  = getdbl(0, 10);
          m.lcb  = getdbl(10, 10, m.lca);
          m.lcc  = getdbl(20, 10, m.lca);
          m.lcs  = getdbl(30, 10, m.lca);
          m.lcab = getdbl(40, 10, m.lcs);
          m.lcbc = getdbl(50, 10, m.lcs);
          m.lcca = getdbl(60, 10, m.lcs);
          m.lcsr = getdbl(70, 10);
          getlin();
          m.eaau = getdbl(0, 10);
          m.ebbu = getdbl(10, 10);
          m.eccu = getdbl(20, 10);
          m.gabu = getdbl(30, 10);
          m.gbcu = getdbl(40, 10);
          m.gcau = getdbl(50, 10);
          m.aopt = getdbl(60, 10);
          m.macf = getint(70, 10);
          getlin();
          m.xp = getdbl(0, 10);
          m.yp = getdbl(10, 10);
          m.zp = getdbl(20, 10);
          m.a1 = getdbl(30, 10);
          m.a2 = getdbl(40, 10);
          m.a3 = getdbl(50, 10);
          getlin();
          m.d1     = getdbl(0, 10);
          m.d2     = getdbl(10, 10);
          m.d3     = getdbl(20, 10);
          m.tsef   = getdbl(30, 10);
          m.ssef   = getdbl(40, 10);
          m.vref   = getdbl(50, 10);
          m.tref   = getdbl(60, 10);
          m.shdflg = getdbl(70, 10);
          materials[m.mid] = m;
          if(m.aopt == 3 || m.aopt == 4) {
            getlin();
            m.v1 = getdbl(0, 10);
            m.v2 = getdbl(10, 10);
            m.v3 = getdbl(20, 10);
          }
          if(m.lcsr == -1) {
            getlin();
            m.lcsra  = getdbl(0, 10);
            m.lcsrb  = getdbl(10, 10);
            m.lcsrc  = getdbl(20, 10);
            m.lcsrab = getdbl(30, 10);
            m.lcsrbc = getdbl(40, 10);
            m.lcsca  = getdbl(50, 10);
          }
        } break;
        case MAT_DAMPER_VISCOUS : {
          Material m;
          m.type = Material::DAMPER_VISCOUS;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid = getint(0, 10);
          m.dc  = getdbl(10, 10);
          materials[m.mid] = m;
        } break;
        case MAT_SPRING_NONLINEAR_ELASTIC : {
          Material m;
          m.type = Material::SPRING_NONLINEAR_ELASTIC;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid = getint(0, 10);
          m.lcd = getint(10, 10);
          m.lcr = getint(20, 10);
          materials[m.mid] = m;
        } break;
        case MAT_SEATBELT : {
          Material m;
          m.type = Material::SEATBELT;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid = getint(0, 10);
          m.mpul = getdbl(10, 10);
          m.llcid = getint(20, 10);
          m.ulcid = getint(30, 10);
          materials[m.mid] = m;
        } break;
        case MAT_FABRIC : {
          Material m;
          m.type = Material::FABRIC;
          if(title) {
            m.title = getstr(0, 80);
            getlin();
          }
          m.mid = getint(0, 10);
          m.ro = getdbl(10, 10);
          m.ea = getdbl(20, 10);
          m.eb = getdbl(30, 10);
          m.prba = getdbl(50, 10);
          m.prab = getdbl(60, 10);
          getlin(); // card 2
          m.gab = getdbl(0, 10);
          getlin(); // card 3
          getlin(); // card 4
          getlin(); // card 5
          materials[m.mid] = m;
        } break;
        case DEFINE_CURVE : {
          Curve c;
          if(title) {
            c.title = getstr(0, 80);
            getlin();
          }
          c.lcid   = getint(0, 10);
          c.sidr   = getint(10, 10);
          c.sfa    = getdbl(20, 10, 1.0);
          c.sfo    = getdbl(30, 10, 1.0);
          c.offa   = getdbl(40, 10);
          c.offo   = getdbl(50, 10);
          c.dattyp = getint(60, 10);
          c.lcint  = getint(70, 10);
          for(int i = 0; i < c.lcint; ++i) {
            getlin();
            c.a.push_back(getdbl(0, 20, 0.0));
            c.o.push_back(getdbl(20, 20, 0.0));
          }
          if(c.lcint == 0) {
            while(true) {
              line_number++;
              std::getline(in, line);
              if(line[0] == '$' || line[0] == '*') {
                read_next = false;
                break;
              }
              c.a.push_back(getdbl(0, 20, 0.0));
              c.o.push_back(getdbl(20, 20, 0.0));
            }
          }
          curves[c.lcid] = c;
          if(table_id > -1 && tables[table_id].curves.size() < tables[table_id].values.size()) {
            // this curve is part of a table
            tables[table_id].curves.push_back(c.lcid);
            if(tables[table_id].curves.size() == tables[table_id].values.size()) table_id = -1;
          }
        } break;
        case DEFINE_TABLE : {
          Table t;
          if(title) {
            t.title = getstr(0, 80);
            getlin();
          }
          t.tbid = getint(0, 10);
          t.sfa  = getdbl(10, 10, 1.0);
          t.offa = getdbl(20, 10, 0.0);
          while(true) {
            line_number++;
            std::getline(in, line);
            if(line[0] == '$' || line[0] == '*') {
              read_next = false;
              break;
            }
            double value = getdbl(0, 10, 0.0);
            t.values.push_back(value);
          }
          tables[t.tbid] = t;
          table_id = t.tbid;
        } break;
        case DEFINE_HEX_SPOTWELD_ASSEMBLY_8 : {
          // TODO
          if(title) {
            getlin();
          }
          getlin(); // Card 2
        } break;
        case BOUNDARY_SPC : {
          Boundary b;
          b.nid = getint(0, 10);
          b.cid = getint(10, 10);
          b.dofx = getint(20, 10);
          b.dofy = getint(30, 10);
          b.dofz = getint(40, 10);
          b.dofrx = getint(50, 10);
          b.dofry = getint(60, 10);
          b.dofrz = getint(70, 10);
          boundaries.push_back(b);
        } break;
        case KEYWORD :
        case END : {
        } break;
        case NOT_TRANSLATED : {
          std::cerr << "line #" << line_number << " not translated: " << line << std::endl;
        } break;
      }
      count[keyword]++;
    }
    if(read_next) {
      line_number++;
      std::getline(in, line);
    }
  }
  in.close();

  std::cerr << "read " << nodes.size() << " nodes, " << elements.size() << " elements (" << count[ELEMENT_BEAM] << " beam + "
            << count[ELEMENT_SHELL] << " shell + " << count[ELEMENT_SOLID] << " solid + " << count[ELEMENT_DISCRETE]+count[ELEMENT_SEATBELT] << " spring), "
            << sections.size() << " sections (" << count[SECTION_SHELL] << " shell + " << count[SECTION_BEAM] << " beam + "
            << count[SECTION_SOLID] << " solid + " << count[SECTION_DISCRETE]+count[SECTION_SEATBELT] << " spring), " << discrete_masses.size()
            << " discrete masses, " << parts.size() << " parts, " << count[CONSTRAINED_NODAL_RIGID_BODY] << " nodal rigid bodies, "
            << rigid_bodies.size() << " constrained rigid bodies, " << constrained_extra_nodes.size() << " constrained extra nodes, "
            << joints.size() << " joints, " << sets.size() << " sets, " << materials.size() << " materials, and " << curves.size()
            << " curves\n";

  // write the header
  std::ofstream out(argv[2]);
  out << "DYNAMICS\n"
      << "newmark\n"
      << "mech 0.0 0.5\n"
      << "time 0 0 1\n"
      << "*\n"
      << "CONSTRAINTS\n"
      << "penalty 1e9\n";

  // renumber and write the nodes 
  out << "*" << std::endl << "NODES" << std::endl;
  double xmin = std::numeric_limits<double>::max();
  double xmax = std::numeric_limits<double>::lowest();
  for(int i = 0; i < nodes.size(); ++i) {
    node_renum[nodes[i].nid] = i+1;
    out << std::right << std::setprecision(8) << std::scientific << std::uppercase
        << std::setw(8)  << i+1 << " " 
        << std::setw(16) << nodes[i].x << " "
        << std::setw(16) << nodes[i].y << " "
        << std::setw(16) << nodes[i].z << std::endl;
    if(nodes[i].x < xmin) xmin = nodes[i].x;
    if(nodes[i].x > xmax) xmax = nodes[i].x;
  }
  std::cerr << "xmin = " << xmin << ", xmax = " << xmax << std::endl;

  // renumber the parts
  for(int i = 0; i < parts.size(); ++i) {
    part_renum[parts[i].pid] = i+1;
  }

  // renumber the sets
  for(int i = 0; i < sets.size(); ++i) {
    set_renum[sets[i].sid] = i+1;
  }

  // check the elements
  for(auto it = elements.begin(); it != elements.end(); ) {
    if(std::all_of(it->n.begin(), it->n.end(), [&](int i){ return node_renum.contains(i); })) {
      it++;
    }
    else {
      std::cerr << "*** Warning: element #" << it->eid << " not translated due to missing nodes\n";
      it = elements.erase(it);
      wrn++;
      continue;
    }
  }

  // renumber and write the elements, also find and store nodes in each rigid part
  std::map<int,std::set<int>> rigid_parts;
  out << "*" << std::endl << "TOPOLOGY" << std::endl;
  for(int i = 0; i < elements.size(); ++i) {
    element_renum[elements[i].eid] = i+1;
    out << std::right << std::setw(8) << i+1 << " ";
    Part& p = parts[part_renum[elements[i].pid]-1];
    Section& s = sections[p.secid];
    Material& m = materials[p.mid];
    if(m.type == Material::RIGID) {
      if(!rigid_bodies.empty() || !constrained_extra_nodes.empty()) {
        rigid_parts[elements[i].pid].insert(elements[i].n[0]);
      }
      switch(elements[i].type) {
        case Element::SHELL : {
          out << std::right << std::setw(8) << ((elements[i].n.size() == 3) ? 73 : 76);
          if(elements[i].n.size() != 3) {
            // check for self intersecting quadrilateral
            Eigen::Matrix<double, 3, 4> xyz;
            for(int j=0; j<4; ++j) {
              xyz.col(j) << nodes[node_renum[elements[i].n[j]]-1].x,
                            nodes[node_renum[elements[i].n[j]]-1].y,
                            nodes[node_renum[elements[i].n[j]]-1].z;
            }
            Eigen::Vector3d n1 = (xyz.col(1)-xyz.col(0)).cross(xyz.col(2)-xyz.col(0)); // normal to triangle 012
            Eigen::Vector3d n2 = (xyz.col(3)-xyz.col(2)).cross(xyz.col(0)-xyz.col(2)); // normal to triangle 230
            if(n1.dot(n2) < 0) {
              std::cerr << "*** Warning: found self-intersecting quadrilateral (EID = " << elements[i].eid << ")\n";
              std::swap(elements[i].n[1], elements[i].n[2]);
            }
          }
        } break;
        case Element::BEAM : {
          // check length
          double& x0 = nodes[node_renum[elements[i].n[0]]-1].x;
          double& x1 = nodes[node_renum[elements[i].n[1]]-1].x;
          double& y0 = nodes[node_renum[elements[i].n[0]]-1].y;
          double& y1 = nodes[node_renum[elements[i].n[1]]-1].y;
          double& z0 = nodes[node_renum[elements[i].n[0]]-1].z;
          double& z1 = nodes[node_renum[elements[i].n[1]]-1].z;
          double len = std::sqrt((x1-x0)*(x1-x0) + (y1-y0)*(y1-y0) + (z1-z0)*(z1-z0));
          if(len == 0) {
            std::cerr << "*** Warning: rigid beam or truss element has zero length (EID = " << elements[i].eid << ")\n";
          }
          out << std::right << std::setw(8) << ((s.elform == 3) ? 65 : 106);
        } break;
        case Element::SOLID : {
          switch(elements[i].n.size()) {
            case 4: {
              out << std::right << std::setw(8) << 150;
              if(elements[i].n.size() == 4) {
                // check volume
                Eigen::Matrix<double, 3, 4> xyz;
                for(int j=0; j<4; ++j) {
                  xyz.col(j) << nodes[node_renum[elements[i].n[j]]-1].x,
                                nodes[node_renum[elements[i].n[j]]-1].y,
                                nodes[node_renum[elements[i].n[j]]-1].z;
                }
                double vol = 1/6.* (xyz.col(3)-xyz.col(0)).dot((xyz.col(1)-xyz.col(0)).cross(xyz.col(2)-xyz.col(0)));
                if(vol == 0) {
                  std::cerr << "*** Warning: tetrahedral element has negative or zero volume (EID = " << elements[i].eid << ")\n";
                }
              }
            } break;
            case 6: {
              out << std::right << std::setw(8) << 151;
            } break;
            case 8: {
              out << std::right << std::setw(8) << 70;
            } break;
            default: {
              std::cerr << "*** Warning: element #" << i+1 << " translated to rigid solid type 71 which has no mass\n"; wrn++;
              out << std::right << std::setw(8) << 71;
            } break;
          }
        } break;
      }
    }
    else if(m.type == Material::DAMPER_VISCOUS || m.type == Material::SPRING_NONLINEAR_ELASTIC) {
      out << std::right << std::setw(8) << 106; // TODO
    }
    else {
      switch(elements[i].type) {
        case Element::SHELL : {
          out << std::right << std::setw(8) << ((elements[i].n.size() == 3) ? 15 : 1515);
          if(elements[i].n.size() != 3) {
            // check for self intersecting quadrilateral
            Eigen::Matrix<double, 3, 4> xyz;
            for(int j=0; j<4; ++j) {
              xyz.col(j) << nodes[node_renum[elements[i].n[j]]-1].x,
                            nodes[node_renum[elements[i].n[j]]-1].y,
                            nodes[node_renum[elements[i].n[j]]-1].z;
            }
            Eigen::Vector3d n1 = (xyz.col(1)-xyz.col(0)).cross(xyz.col(2)-xyz.col(0)); // normal to triangle 012
            Eigen::Vector3d n2 = (xyz.col(3)-xyz.col(2)).cross(xyz.col(0)-xyz.col(2)); // normal to triangle 230
            if(n1.dot(n2) <= 0) {
              std::cerr << "*** Warning: found self-intersecting quadrilateral (EID = " << elements[i].eid << ")\n";
              std::swap(elements[i].n[1], elements[i].n[2]);
            }
          }
        } break;
        case Element::BEAM : {
          // check length
          double& x0 = nodes[node_renum[elements[i].n[0]]-1].x;
          double& x1 = nodes[node_renum[elements[i].n[1]]-1].x;
          double& y0 = nodes[node_renum[elements[i].n[0]]-1].y;
          double& y1 = nodes[node_renum[elements[i].n[1]]-1].y;
          double& z0 = nodes[node_renum[elements[i].n[0]]-1].z;
          double& z1 = nodes[node_renum[elements[i].n[1]]-1].z;
          double len = std::sqrt((x1-x0)*(x1-x0) + (y1-y0)*(y1-y0) + (z1-z0)*(z1-z0));
          if(len == 0) {
            std::cerr << "*** Warning: beam or truss element has zero length (EID = " << elements[i].eid << ")\n";
            // TODO: element 114 is massless so it may not work for explicit
            out << std::right << std::setw(8) << ((s.elform == 3) ? 114 : 106);
          }
          else {
            out << std::right << std::setw(8) << ((s.elform == 3) ? 1 : 6);
          }
        } break;
        case Element::SOLID : {
          out << std::right << std::setw(8) << ((elements[i].n.size() == 4) ? 23 : ((elements[i].n.size() == 6) ? 24 : 17));
          if(elements[i].n.size() == 4) {
            // check volume
            Eigen::Matrix<double, 3, 4> xyz;
            for(int j=0; j<4; ++j) {
              xyz.col(j) << nodes[node_renum[elements[i].n[j]]-1].x,
                            nodes[node_renum[elements[i].n[j]]-1].y,
                            nodes[node_renum[elements[i].n[j]]-1].z;
            }
            double vol = 1/6.* (xyz.col(3)-xyz.col(0)).dot((xyz.col(1)-xyz.col(0)).cross(xyz.col(2)-xyz.col(0)));
            if(vol == 0) {
              std::cerr << "*** Warning: tetrahedral element has negative or zero volume (EID = " << elements[i].eid << ")\n";
            }
          }
        } break;
        case Element::DISCRETE : {
          out << std::right << std::setw(8) << ((s.dro == 0) ? 201 : 202);
        } break;
        case Element::SEATBELT : {
          out << std::right << std::setw(8) << 204;
        } break;
      }
    }
    for(int j : elements[i].n) {
      out << " " << std::right << std::setw(8) << node_renum[j];
    }
    out << std::endl;
  }

  // check the joints
  for(auto it = joints.begin(); it != joints.end(); ) {
    if(node_renum.contains(it->n1) && node_renum.contains(it->n2)) {
      it++;
    }
    else {
      std::cerr << "*** Warning: joint #" << it->jid << " not translated due to missing nodes\n";
      it = joints.erase(it);
      wrn++;
      continue;
    }
  }

  // renumber and write the joints
  for(int i = 0; i < joints.size(); ++i) {
    joint_renum[joints[i].jid] = elements.size()+i+1;
    out << std::right << std::setw(8) << elements.size()+i+1 << " ";
    switch(joints[i].type) {
      case Joint::SPHERICAL   : out << std::right << std::setw(8) << 120; break;
      case Joint::UNIVERSAL   : out << std::right << std::setw(8) << 122; break;
      case Joint::REVOLUTE    : out << std::right << std::setw(8) << 123; break;
      case Joint::CYLINDRICAL : out << std::right << std::setw(8) << 124; break;
      case Joint::SPOTWELD    : out << std::right << std::setw(8) << 106; break;
    }
    out << " " << std::right << std::setw(8) << node_renum[joints[i].n1];
    out << " " << std::right << std::setw(8) << node_renum[joints[i].n2];
    out << std::endl;
  }

  // check the rigid elements
  for(auto it = rigid_elements.begin(); it != rigid_elements.end(); ) {
    if(set_renum.contains(it->nsid)) {
      Set& s = sets[set_renum[it->nsid]-1];
      if(s.type != Set::NODE) continue;
      if(std::all_of(s.list.begin(), s.list.end(), [&](int i){ return node_renum.contains(i); })) {
        it++;
      }
      else {
        std::cerr << "*** Warning: constrained nodal rigid body not translated due to missing nodes\n";
        it = rigid_elements.erase(it);
        wrn++;
        continue;
      }
    }
    else {
      std::cerr << "*** Warning: constrained nodal rigid body not translated due to missing node set: NSID = " << it->nsid << std::endl;
      it = rigid_elements.erase(it);
      wrn++;
      continue;
    }
  }

  // write the rigid elements
  int rigid_element_count = 0;
  for(int i = 0; i < rigid_elements.size(); ++i) {
    Set& s = sets[set_renum[rigid_elements[i].nsid]-1];
    for(int j = 1; j < s.list.size(); ++j) {
      out << std::right
          << std::setw(8) << elements.size()+joints.size()+rigid_element_count+1 << " "
          << std::setw(8) << 106 << " "
          << std::setw(8) << node_renum[s.list[0]] << " "
          << std::setw(8) << node_renum[s.list[j]] << std::endl;
      rigid_element_count++;
    }
  }

  // write the rigid bodies
  int rigid_body_count = 0;
  for(int i = 0; i < rigid_bodies.size(); ++i) {
    if(rigid_parts.find(rigid_bodies[i].pidm) == rigid_parts.end()) {
      std::cerr << "*** Warning: rigid body #" << i+1 << " has missing part (PIDM = " << rigid_bodies[i].pidm << ")\n";
      wrn++;
      continue;
    }
    if(rigid_parts.find(rigid_bodies[i].pids) == rigid_parts.end()) {
      std::cerr << "*** Warning: rigid body #" << i+1 << " has missing part (PIDS = " << rigid_bodies[i].pids << ")\n";
      wrn++;
      continue;
    }
    int nidm = *(rigid_parts[rigid_bodies[i].pidm].begin());
    int nids = *(rigid_parts[rigid_bodies[i].pids].begin());
    // add a rigid beam connecting the first node of the master part to the first node of the slave part
    // note: another option would be to find and connect the closest nodes
    out << std::right << std::setw(8) << elements.size()+joints.size()+rigid_element_count+rigid_body_count+1 << " ";
    out << std::right << std::setw(8) << 106;
    out << " " << std::right << std::setw(8) << node_renum[nidm];
    out << " " << std::right << std::setw(8) << node_renum[nids] << std::endl;
    rigid_body_count++;
  }

  // write the constrained extra nodes
  int constrained_extra_node_count = 0;
  for(int i = 0; i < constrained_extra_nodes.size(); ++i) {
    if(rigid_parts.find(constrained_extra_nodes[i].pid) == rigid_parts.end()) {
      std::cerr << "*** Warning: constrained extra nodes #" << i+1 << " has missing part (PID = " << constrained_extra_nodes[i].pid << ")\n";
      wrn++;
      continue;
    }
    int nidm = *(rigid_parts[constrained_extra_nodes[i].pid].begin());
    if(constrained_extra_nodes[i].nid > -1) {
      int nids = constrained_extra_nodes[i].nid;
      out << std::right << std::setw(8) << elements.size()+joints.size()+rigid_element_count+rigid_body_count+constrained_extra_node_count+1 << " ";
      out << std::right << std::setw(8) << 106;
      out << " " << std::right << std::setw(8) << node_renum[nidm];
      out << " " << std::right << std::setw(8) << node_renum[nids] << std::endl;
      constrained_extra_node_count++;
    }
    else if(constrained_extra_nodes[i].nsid > -1) {
      if(set_renum.find(constrained_extra_nodes[i].nsid) == set_renum.end()) {
        std::cerr << "*** Warning constrained extra nodes #" << i+1 << " has missing node set (NSID = " << constrained_extra_nodes[i].nsid << ")\n";
        wrn++;
        continue;
      }
      Set& s = sets[set_renum[constrained_extra_nodes[i].nsid]-1];
      for(int nids : s.list) {
        out << std::right << std::setw(8) << elements.size()+joints.size()+rigid_element_count+rigid_body_count+constrained_extra_node_count+1 << " ";
        out << std::right << std::setw(8) << 106;
        out << " " << std::right << std::setw(8) << node_renum[nidm];
        out << " " << std::right << std::setw(8) << node_renum[nids] << std::endl;
        constrained_extra_node_count++;
      }
    } 
  }

  // write the attributes
  out << "*" << std::endl << "ATTRIBUTES" << std::endl;
  for(int i = 0; i < elements.size(); ++i) {
    if(part_renum.find(elements[i].pid) == part_renum.end()) {
      std::cerr << "*** Warning: element #" << elements[i].eid << " has missing part\n";
      wrn++;
      continue;
    }
    out << std::right
        << std::setw(8) << i+1 << " "
        << std::setw(8) << part_renum[elements[i].pid] << std::endl;
  }
  // write attributes for joints
  for(int i = 0; i < joints.size(); ++i) {
    out << std::right
        << std::setw(8) << elements.size()+i+1 << " "
        << std::setw(8) << parts.size()+1 << std::endl;
  }
  // write attributes for rigid elements
  rigid_element_count = 0;
  for(int i = 0; i < rigid_elements.size(); ++i) {
    Set& s = sets[set_renum[rigid_elements[i].nsid]-1];
    for(int j = 1; j < s.list.size(); ++j) {
      out << std::right
          << std::setw(8) << elements.size()+joints.size()+rigid_element_count+1 << " "
          << std::setw(8) << parts.size()+1 << std::endl;
      rigid_element_count++;
    }
  }
  // write attributes for rigid bodies
  rigid_body_count = 0;
  for(int i = 0; i < rigid_bodies.size(); ++i) {
    if(rigid_parts.find(rigid_bodies[i].pidm) == rigid_parts.end()) {
      continue;
    }
    if(rigid_parts.find(rigid_bodies[i].pids) == rigid_parts.end()) {
      continue;
    }
    out << std::right
        << std::setw(8) << elements.size()+joints.size()+rigid_element_count+rigid_body_count+1 << " "
        << std::setw(8) << parts.size()+1 << std::endl;
    rigid_body_count++;
  }
  // write attributes for constrained extra nodes
  constrained_extra_node_count = 0;
  for(int i = 0; i < constrained_extra_nodes.size(); ++i) {
    if(rigid_parts.find(constrained_extra_nodes[i].pid) == rigid_parts.end()) {
      continue;
    }
    bool needs_attr = part_renum.contains(constrained_extra_nodes[i].pid);
    if(constrained_extra_nodes[i].nid > -1) {
      if(needs_attr) {
        out << std::right << std::setw(8) << elements.size()+joints.size()+rigid_element_count+rigid_body_count+constrained_extra_node_count+1 << " ";
        out << std::right << std::setw(8) << part_renum[constrained_extra_nodes[i].pid] << std::endl;
      }
      constrained_extra_node_count++;
    }
    else if(constrained_extra_nodes[i].nsid > -1) {
      if(set_renum.find(constrained_extra_nodes[i].nsid) == set_renum.end()) {
        continue;
      }
      Set& s = sets[set_renum[constrained_extra_nodes[i].nsid]-1];
      for(int nids : s.list) {
        if(needs_attr) {
          out << std::right << std::setw(8) << elements.size()+joints.size()+rigid_element_count+rigid_body_count+constrained_extra_node_count+1 << " ";
          out << std::right << std::setw(8) << part_renum[constrained_extra_nodes[i].pid] << std::endl;
        }
        constrained_extra_node_count++;
      }
    }
  }

  // write the material properties
  out << "*" << std::endl << "MATERIALS" << std::endl;
  for(int i = 0; i < parts.size(); ++i) {
    if(sections.find(parts[i].secid) == sections.end()) {
      std::cerr << "*** Warning: part #" << parts[i].pid << " not translated due to missing section\n";;
      wrn++;
      continue;
    }
    if(materials.find(parts[i].mid) == materials.end()) {
      std::cerr << "*** Warning: part #" << parts[i].pid << " not translated due to missing material\n";;
      wrn++;
      continue;
    }
    Section& s = sections[parts[i].secid];
    Material& m = materials[parts[i].mid];

    double A = 0;    // cross sectional area for trusses and beams
    double E = 0;    // Young's Modulus
    double nu = 0;   // Poisson's ratio
    double rho = 0;  // mass density per unit volume accounting for both structural and non-structural types of mass (some exceptions)
    double h = 0;    // heat convection coefficient
    double k = 0;    // heat conduction coefficient 
    double t = 0;    // element thickness
    double P = 0;    // perimeter/circumference area for thermal elements
    double Ta = 0;   // reference temperature
    double cp = 0;   // specific heat coefficient
    double w = 0;    // coefficient of thermal expansion 
    double Ixx = 0;  // cross-sectional moment of inertia about the local x-axis
    double Iyy = 0;  // cross-sectional moment of inertia about the local y-axis
    double Izz = 0;  // cross-sectional moment of inertia about the local z-axis
    double ymin = 0; // negative local y-coordinate of the bottom fiber of a beam cross section
    double ymax = 0; // positive local y-coordinate of the top fiber of a beam cross section
    double zmin = 0; // negative local z-coordinate of the bottom fiber of a beam cross section
    double zmax = 0; // positive local z-coordinate of the top fiber of a beam cross section
    switch(m.type) {
      case Material::ELASTIC :
      case Material::PLASTIC_KINEMATIC :
      case Material::RIGID :
      case Material::PIECEWISE_LINEAR_PLASTICITY :
      case Material::SPOTWELD :
      case Material::MODIFIED_PIECEWISE_LINEAR_PLASTICITY :
      case Material::MODIFIED_HONEYCOMB : {
        E = m.e;
        nu = m.pr;
        rho = m.ro;
      } break;
      case Material::NONLINEAR_ELASTIC_DISCRETE_BEAM :
      case Material::NONLINEAR_PLASTIC_DISCRETE_BEAM : {
        E = 2.07e+05; // TODO
        nu = 0.3;     // TODO
        rho = m.ro;
      } break;
      case Material::DAMPER_VISCOUS :
      case Material::SPRING_NONLINEAR_ELASTIC : {
        rho = m.ro;
      } break;
      case Material::FABRIC : {
        if(m.eb == 0) { // isotropic
          E = m.ea;
          nu = m.prba;
        }
        else {
          E = nu = 0;
          std::cerr << "*** Warning: unsupported MAT_FABRIC option (EB = " << m.eb << ")\n";
        }
        rho = m.ro;
      } break;
      case Material::SEATBELT : {
        rho = m.mpul/100;
      }
    }
    switch(s.type) {
      case Section::SHELL : {
        t = s.t1;
      } break;
      case Section::BEAM : {
        if(s.elform == 3) { // truss
          A = s.a;
          Ixx = 0;
          Iyy = 0;
          Izz = 0;
        }
        else if((s.elform == 1 || s.elform == 9 || s.elform == 11) && (s.cst == 0)) { // beam with rectangular cross section
          double ts = (s.ts1 + s.ts2)/2; // average thickness in t direction
          double tt = (s.tt1 + s.tt2)/2; // average thickness in t direction
          A = ts*tt;
          Iyy = tt*ts*ts*ts/12;
          Izz = ts*tt*tt*tt/12;
          Ixx = Iyy + Izz;
        }
        else if((s.elform == 1 || s.elform == 9 || s.elform == 11) && (s.cst == 1)) { // beam with tubular cross section
          double rout = (s.ts1 + s.ts2)/4; // average outer radius
          double rin = (s.tt1 + s.tt2)/4; // average inner radius
          A = M_PI*(rout*rout - rin*rin);
          Iyy = Izz = M_PI*(rout*rout*rout*rout - rin*rin*rin*rin);
          Ixx = Iyy + Izz;
        }
        else {
          std::cerr << "*** Warning: unsupported beam section (ELFORM = " << s.elform << ", CST = " << s.cst << ")\n";
          A = 100; Iyy = Izz = 833.333333333; Ixx = Iyy + Izz;
        }
      } break;
      case Section::DISCRETE : {
        A = 100;
      } break;
      case Section::SEATBELT : {
        A = 100;
      } break;
    }
    if(m.type == Material::RIGID && A == 0) {
      A = 100; // this is for rigid beams connecting constrained extra nodes to part
    }
    out << std::right << std::setprecision(8) << std::scientific << std::uppercase
        << std::setw(8)  << i+1 << " ";
    if(m.type == Material::SEATBELT) {
      Curve& c = curves[m.llcid];
      double a0 = c.offa + c.sfa * c.a[0];
      double a1 = c.offa + c.sfa * c.a[1];
      double o0 = c.offo + c.sfo * c.o[0];
      double o1 = c.offo + c.sfo * c.o[1];
      out << "springmat " << (o1 - o0)/(a1 - a0) << std::endl;
    }
    else {
      if(A == 0) out << "0 "; else out << std::setw(14) << A << " ";
      if(E == 0) out << "0 "; else out << std::setw(14) << E << " ";
      if(nu == 0) out << "0 "; else out << std::setw(14) << nu << " ";
      if(rho == 0) out << "0 "; else out << std::setw(14) << rho << " ";
      if(h == 0) out << "0 "; else out << std::setw(14) << h << " ";
      if(k == 0) out << "0 "; else out << std::setw(14) << k << " ";
      if(t == 0) out << "0 "; else out << std::setw(14) << t << " ";
      if(P == 0) out << "0 "; else out << std::setw(14) << P << " ";
      if(Ta == 0) out << "0 "; else out << std::setw(14) << Ta << " ";
      if(cp == 0) out << "0 "; else out << std::setw(14) << cp << " ";
      if(w == 0) out << "0 "; else out << std::setw(14) << w << " ";
      if(Ixx == 0) out << "0 "; else out << std::setw(14) << Ixx << " ";
      if(Iyy == 0) out << "0 "; else out << std::setw(14) << Iyy << " ";
      if(Izz == 0) out << "0 "; else out << std::setw(14) << Izz << " ";
      if(ymin == 0) out << "0 "; else out << std::setw(14) << ymin << " ";
      if(ymax == 0) out << "0 "; else out << std::setw(14) << ymax << " ";
      if(zmin == 0) out << "0 "; else out << std::setw(14) << zmin << " ";
      if(zmax == 0) out << "0\n"; else out << std::setw(14) << zmax << std::endl;
    }
  }
  // write extra material for constrained nodal rigid bodies (TODO)
  /*out << std::right << std::setprecision(8) << std::scientific << std::uppercase
      << std::setw(8)  << parts.size()+1 << " conmat mass "
      << std::setw(16) << 7.85000000E-09 << " "
      << std::setw(16) << 100 << std::endl;*/

  // write the nonlinear material usage
  bool first_time = true;
  for(int i = 0; i < elements.size(); ++i) {
    if(part_renum.find(elements[i].pid) == part_renum.end() ||
       sections.find(parts[part_renum[elements[i].pid]-1].secid) == sections.end() ||
       materials.find(parts[part_renum[elements[i].pid]-1].mid) == materials.end()) continue;
    Section& s = sections[parts[part_renum[elements[i].pid]-1].secid];
    Material& m = materials[parts[part_renum[elements[i].pid]-1].mid];
    if(s.type == Section::SHELL && (
        m.type == Material::PLASTIC_KINEMATIC ||
        m.type == Material::PIECEWISE_LINEAR_PLASTICITY ||
        m.type == Material::MODIFIED_PIECEWISE_LINEAR_PLASTICITY)) { 
      if(first_time) { out << "*" << std::endl << "MATUSAGE" << std::endl; first_time = false; }
      out << std::right
          << std::setw(8) << i+1 << " "
          << std::setw(8) << part_renum[elements[i].pid] << std::endl;
    }
  }

  // write the nonlinear material laws
  first_time = true;
  for(int i = 0; i < parts.size(); ++i) {
    if(sections.find(parts[i].secid) == sections.end() ||
       materials.find(parts[i].mid) == materials.end()) continue;
    Section& s = sections[parts[i].secid];
    Material& m = materials[parts[i].mid];
    if(s.type == Section::SHELL && (
        m.type == Material::PLASTIC_KINEMATIC ||
        m.type == Material::PIECEWISE_LINEAR_PLASTICITY ||
        m.type == Material::MODIFIED_PIECEWISE_LINEAR_PLASTICITY)) {
      if(first_time) { out << "*" << std::endl << "MATLAW" << std::endl; first_time = false; }
      double E = m.e;                          // Young's modulus
      double nu = m.pr;                        // Poisson's ratio
      double rho = m.ro;                       // mass density
      double epsilon = 1e-10;                  // convergence tol
      if(m.type == Material::PLASTIC_KINEMATIC || m.lcss == 0) {
        double sigY = m.sigy;                    // yield stress
        double Et = m.etan;                      // tangent modulus
        double beta = m.beta;                    // hardening parameter
        double K = beta * E * Et / (E - Et);     // isotropic hardening modulus 
        double H = (1-beta) * E * Et / (E - Et); // kinematic hardening modulus
        out << std::right << std::setprecision(8) << std::scientific << std::uppercase
            << std::setw(8) << i+1 << " J2Plasticity "
            << std::setw(16) << E << " "       // Young's modulus
            << std::setw(16) << nu << " "      // Poisson's ratio
            << std::setw(16) << rho << " "     // mass density
            << std::setw(16) << sigY << " "    // yield stress
            << std::setw(16) << K << " "       // isotropic hardening modulus
            << std::setw(16) << H << " "       // kinematic hardening modulus
            << std::setw(16) << epsilon << " " // convergence tol (default 1e-6)
            << std::endl;
      }
      else {
        int ysst_id = -m.lcss;   // curve id defining yield stress vs effective plastic strain
        curves_usage[m.lcss] = Curve::YSST;
        double K = 0;            // isotropic hardening modulus (not used when ysst_id is specified)
        double H = 0;            // kinematic hardening modulus (not used when ysst_id is specified)
        out << std::right << std::setprecision(8) << std::scientific << std::uppercase
            << std::setw(8) << i+1 << " J2Plasticity "
            << std::setw(16) << E << " "       // Young's modulus
            << std::setw(16) << nu << " "      // Poisson's ratio
            << std::setw(16) << rho << " "     // mass density
            << std::setw(8)  << ysst_id << " " // yield stress vs effecive plastic strain curve id (if negative)
            << std::setw(16) << K << " "       // isotropic hardening modulus
            << std::setw(16) << H << " "       // kinematic hardening modulus
            << std::setw(16) << epsilon << " " // convergence tol (default 1e-6)
            << std::endl;
      }
    }
  }

  // group nodes by part
  std::vector<std::set<int>> part_to_node(parts.size());
  for(auto& e : elements) {
    int i = part_renum[e.pid]-1; // part index
    for(int nid : e.n) {
      part_to_node[i].insert(nid);
    }
  }
  for(auto& c : constrained_extra_nodes) {
    if(part_renum.contains(c.pid)) {
      int i = part_renum[c.pid]-1; // part index
      if(c.nid > -1) {
        part_to_node[i].insert(c.nid);
      }
      else if(c.nsid > -1) {
        Set& s = sets[set_renum[c.nsid]-1];
        for(int nid : s.list) {
          part_to_node[i].insert(nid);
        }
      }
    }
  }

  // finalize node sets
  for(auto& s : sets) {
    for(int pid : s.parts) {
      int i = part_renum[pid]-1; // part index;
      for(int nid : part_to_node[i]) {
        s.list.push_back(nid);
      }
    }
  }

  // write the discrete masses
  if(discrete_masses.size() > 0 || std::any_of(parts.begin(), parts.end(), [](Part& p){return p.tm != 0;})) {
    out << "*" << std::endl << "DIMASS" << std::endl;
  }
  for(auto& d : discrete_masses) {
    if(d.type == DiscreteMass::NODE) {
      out << std::right << std::setprecision(8) << std::scientific << std::uppercase
          << std::setw(8)  << node_renum[d.id] << " "
          << std::setw(8)  << 1 << " "
          << std::setw(16) << d.mass << std::endl
          << std::setw(8)  << node_renum[d.id] << " "
          << std::setw(8)  << 2 << " "
          << std::setw(16) << d.mass << std::endl
          << std::setw(8)  << node_renum[d.id] << " "
          << std::setw(8)  << 3 << " "
          << std::setw(16) << d.mass << std::endl;
    }
    else if(d.type == DiscreteMass::NODE_SET) {
      if(set_renum.find(d.id) == set_renum.end()) {
        std::cerr << "*** Warning: node set for ELEMENT_MASS_NODE_SET not found\n";
        wrn++;
        continue;
      }
      Set& s = sets[set_renum[d.id] - 1];
      for(int& nid : s.list) {
        out << std::right << std::setprecision(8) << std::scientific << std::uppercase
            << std::setw(8)  << node_renum[nid] << " "
            << std::setw(8)  << 1 << " "
            << std::setw(16) << d.mass << std::endl
            << std::setw(8)  << node_renum[nid] << " "
            << std::setw(8)  << 2 << " "
            << std::setw(16) << d.mass << std::endl
            << std::setw(8)  << node_renum[nid] << " "
            << std::setw(8)  << 3 << " "
            << std::setw(16) << d.mass << std::endl;
      }
    }
    else if(d.type == DiscreteMass::PART) {
      if(part_renum.find(d.id) == part_renum.end()) {
        std::cerr << "*** Warning: part for ELEMENT_MASS_PART not found\n";
        wrn++;
        continue;
      }
      std::set<int>& nodes = part_to_node[part_renum[d.id] - 1];
      for(int nid : nodes) {
        out << std::right << std::setprecision(8) << std::scientific << std::uppercase
            << std::setw(8)  << node_renum[nid] << " "
            << std::setw(8)  << 1 << " "
            << std::setw(16) << d.mass/nodes.size() << std::endl
            << std::setw(8)  << node_renum[nid] << " "
            << std::setw(8)  << 2 << " "
            << std::setw(16) << d.mass/nodes.size() << std::endl
            << std::setw(8)  << node_renum[nid] << " "
            << std::setw(8)  << 3 << " "
            << std::setw(16) << d.mass/nodes.size() << std::endl;
      }
    }
    else if(d.type == DiscreteMass::PART_SET) {
      if(set_renum.find(d.id) == set_renum.end()) {
        std::cerr << "*** Warning: part set for ELEMENT_MASS_PART_SET not found\n";
        wrn++;
        continue;
      }
      Set& s = sets[set_renum[d.id] - 1];
      for(int& pid : s.parts) {
        if(part_renum.find(d.id) == part_renum.end()) {
          std::cerr << "*** Warning: part for ELEMENT_MASS_PART_SET not found\n";
          wrn++;
          continue;
        }
        std::set<int>& nodes = part_to_node[part_renum[pid] - 1];
        for(int nid : nodes) {
          out << std::right << std::setprecision(8) << std::scientific << std::uppercase
              << std::setw(8)  << node_renum[nid] << " "
              << std::setw(8)  << 1 << " "
              << std::setw(16) << d.mass/nodes.size() << std::endl
              << std::setw(8)  << node_renum[nid] << " "
              << std::setw(8)  << 2 << " "
              << std::setw(16) << d.mass/nodes.size() << std::endl
              << std::setw(8)  << node_renum[nid] << " "
              << std::setw(8)  << 3 << " "
              << std::setw(16) << d.mass/nodes.size() << std::endl;
        }
      }
    }
  }
  for(auto& p : parts) {
    if(p.tm != 0) {
      std::set<int>& nodes = part_to_node[part_renum[p.pid] - 1];
      for(int nid : nodes) {
        out << std::right << std::setprecision(8) << std::scientific << std::uppercase
            << std::setw(8)  << node_renum[nid] << " "
            << std::setw(8)  << 1 << " "
            << std::setw(16) << p.tm/nodes.size() << std::endl
            << std::setw(8)  << node_renum[nid] << " "
            << std::setw(8)  << 2 << " "
            << std::setw(16) << p.tm/nodes.size() << std::endl
            << std::setw(8)  << node_renum[nid] << " "
            << std::setw(8)  << 3 << " "
            << std::setw(16) << p.tm/nodes.size() << std::endl
            << std::setw(8)  << node_renum[nid] << " "
            << std::setw(8)  << 4 << " "
            << std::setw(16) << p.ixx/nodes.size() << std::endl // TODO: need to transform inertia tensor to global frame and subtract contrib of tm
            << std::setw(8)  << node_renum[nid] << " "
            << std::setw(8)  << 5 << " "
            << std::setw(16) << p.iyy/nodes.size() << std::endl // TODO
            << std::setw(8)  << node_renum[nid] << " "
            << std::setw(8)  << 6 << " "
            << std::setw(16) << p.izz/nodes.size() << std::endl; // TODO
      } 
    }
  }

  // write the boundary conditions
  for(int i = 0; i < nodes.size(); ++i) {
    if(nodes[i].tc != 0 || nodes[i].rc != 0) {
      out << "*" << std::endl << "DISPLACEMENTS" << std::endl;
      break;
    }
  }
  for(int i = 0; i < nodes.size(); ++i) {
    switch(nodes[i].tc) {
      case 0:
        break;
      case 1:
        out << i+1 << " 1 0.0" << std::endl;
        break;
      case 2:
        out << i+1 << " 2 0.0" << std::endl;
        break;
      case 3:
        out << i+1 << " 3 0.0" << std::endl;
        break;
      case 4:
        out << i+1 << " 1 0.0" << std::endl
            << i+1 << " 2 0.0" << std::endl;
        break;
      case 5:
        out << i+1 << " 2 0.0" << std::endl
            << i+1 << " 3 0.0" << std::endl;
        break;
      case 6:
        out << i+1 << " 1 0.0" << std::endl
            << i+1 << " 3 0.0" << std::endl;
        break;
      case 7:
        out << i+1 << " 1 0.0" << std::endl
            << i+1 << " 2 0.0" << std::endl
            << i+1 << " 3 0.0" << std::endl;
        break;
    }
    switch(nodes[i].rc) {
      case 0:
        break;
      case 1:
        out << i+1 << " 4 0.0" << std::endl;
        break;
      case 2:
        out << i+1 << " 5 0.0" << std::endl;
        break;
      case 3:
        out << i+1 << " 6 0.0" << std::endl;
        break;
      case 4:
        out << i+1 << " 4 0.0" << std::endl
            << i+1 << " 5 0.0" << std::endl;
        break;
      case 5:
        out << i+1 << " 5 0.0" << std::endl
            << i+1 << " 6 0.0" << std::endl;
        break;
      case 6:
        out << i+1 << " 4 0.0" << std::endl
            << i+1 << " 6 0.0" << std::endl;
        break;
      case 7:
        out << i+1 << " 4 0.0" << std::endl
            << i+1 << " 5 0.0" << std::endl
            << i+1 << " 6 0.0" << std::endl;
        break;
    }
  }

  // write the element frames
  first_time = true;
  for(int i = 0; i < joints.size(); ++i) {
    if(joints[i].type == Joint::SPHERICAL || joints[i].type == Joint::SPOTWELD) continue;
    Node& n1 = nodes[node_renum[joints[i].n1]-1];
    Node& n2 = nodes[node_renum[joints[i].n2]-1];
    Node& n3 = nodes[node_renum[joints[i].n3]-1];
    Node& n4 = nodes[node_renum[joints[i].n4]-1];

    double c0[3][3];
    c0[0][0] = n3.x - n1.x;
    c0[0][1] = n3.y - n1.y;
    c0[0][2] = n3.z - n1.z;
    double N = sqrt(c0[0][0]*c0[0][0] + c0[0][1]*c0[0][1] + c0[0][2]*c0[0][2]);
    for(int j = 0; j < 3; ++j) c0[0][j] /= N; 
    double N1 = sqrt(c0[0][0]*c0[0][0] + c0[0][1]*c0[0][1]);
    double N2 = sqrt(c0[0][0]*c0[0][0] + c0[0][2]*c0[0][2]);
    if(N1 > N2) {
      c0[1][0] = -c0[0][1]/N1;
      c0[1][1] = c0[0][0]/N1;
      c0[1][2] = 0.0;
    }
    else {
      c0[1][0] = c0[0][2]/N2;
      c0[1][1] = 0.0;
      c0[1][2] = -c0[0][0]/N2;
    }
    c0[2][0] = c0[0][1] * c0[1][2] - c0[0][2] * c0[1][1];
    c0[2][1] = c0[0][2] * c0[1][0] - c0[0][0] * c0[1][2];
    c0[2][2] = c0[0][0] * c0[1][1] - c0[0][1] * c0[1][0];

    if(first_time) { out << "*" << std::endl << "EFRAMES" << std::endl; first_time = false; }
    out << std::right << std::setprecision(8) << std::scientific << std::uppercase
        << joint_renum[joints[i].jid]
        << " " << c0[0][0] << " " << c0[0][1] << " " << c0[0][2] 
        << " " << c0[1][0] << " " << c0[1][1] << " " << c0[1][2]
        << " " << c0[2][0] << " " << c0[2][1] << " " << c0[2][2] << std::endl;
  }

  // write the curves
  for(auto& p : curves_usage) {
    Curve& c = curves.contains(p.first) ? curves[p.first] : curves[tables[p.first].curves[0]]; // TODO: support for multi-curve tables
    int lcid = curves.contains(p.first) ? curves[p.first].lcid : tables[p.first].tbid;
    switch(p.second) {
      case Curve::MFTT : {
        out << "*" << std::endl << "MFTT" << std::endl
            << "curve " << lcid << std::endl;
      } break;
      case Curve::YSST : {
        out << "*" << std::endl << "YSST" << std::endl
            << "curve " << lcid << std::endl;
      } break;
      case Curve::YSSRT : {
        out << "*" << std::endl << "YSSRT" << std::endl
            << "curve " << lcid << std::endl;
      } break;
    }
    for(int i = 0; i < c.a.size(); ++i) {
       out << std::right << std::setprecision(8) << std::scientific << std::uppercase
            << std::setw(16) << c.offa + c.sfa * c.a[i] << " "
            << std::setw(16) << c.offo + c.sfo * c.o[i] << std::endl;
    }
  }

  // write the boundary conditions
  if(boundaries.size() > 0) out << "*" << std::endl << "DISPLACEMENTS" << std::endl;
  for(auto& b : boundaries) {
    if(b.dofx == 1) out << std::right << std::setw(8) << node_renum[b.nid] << " 1 0.0\n";
    if(b.dofy == 1) out << std::right << std::setw(8) << node_renum[b.nid] << " 2 0.0\n";
    if(b.dofz == 1) out << std::right << std::setw(8) << node_renum[b.nid] << " 3 0.0\n";
    if(b.dofrx == 1) out << std::right << std::setw(8) << node_renum[b.nid] << " 4 0.0\n";
    if(b.dofry == 1) out << std::right << std::setw(8) << node_renum[b.nid] << " 5 0.0\n";
    if(b.dofrz == 1) out << std::right << std::setw(8) << node_renum[b.nid] << " 6 0.0\n";
  }

  out << "*" << std::endl << "END" << std::endl;
  out.close();
  std::cerr << "finished translation\n";

  return 0;
}
